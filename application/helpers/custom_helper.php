<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if(!function_exists('datebr')){

	function datebr($data){
		
		$newDate = date("d/m/Y", strtotime($data));
		return $newDate;
	}

}

if(!function_exists('date2mysql')){

	function date2mysql($data){
		
		$newDate = date("Y-m-d", strtotime($data));
		return $newDate;
	}

}
if(!function_exists('limitarTexto')){
	
	function limitarTexto($texto, $limite){
	    $texto = substr($texto, 0, strrpos(substr($texto, 0, $limite), ' ')) . '...';
	    return $texto;
	}
}
if(!function_exists('formataValor')){
	
	function formataValor($valor){	   	
		$valor = "R$ ".number_format($valor, 2, ',', '.');
	    return $valor;
	}
}
if(!function_exists('slugfy')){
	
	function slugify($string) {
	    $string = preg_replace('/[\t\n]/', ' ', $string);
	    $string = preg_replace('/\s{2,}/', ' ', $string);
	    $list = array(
	        'Š' => 'S',
	        'š' => 's',
	        'Đ' => 'Dj',
	        'đ' => 'dj',
	        'Ž' => 'Z',
	        'ž' => 'z',
	        'Č' => 'C',
	        'č' => 'c',
	        'Ć' => 'C',
	        'ć' => 'c',
	        'À' => 'A',
	        'Á' => 'A',
	        'Â' => 'A',
	        'Ã' => 'A',
	        'Ä' => 'A',
	        'Å' => 'A',
	        'Æ' => 'A',
	        'Ç' => 'C',
	        'È' => 'E',
	        'É' => 'E',
	        'Ê' => 'E',
	        'Ë' => 'E',
	        'Ì' => 'I',
	        'Í' => 'I',
	        'Î' => 'I',
	        'Ï' => 'I',
	        'Ñ' => 'N',
	        'Ò' => 'O',
	        'Ó' => 'O',
	        'Ô' => 'O',
	        'Õ' => 'O',
	        'Ö' => 'O',
	        'Ø' => 'O',
	        'Ù' => 'U',
	        'Ú' => 'U',
	        'Û' => 'U',
	        'Ü' => 'U',
	        'Ý' => 'Y',
	        'Þ' => 'B',
	        'ß' => 'Ss',
	        'à' => 'a',
	        'á' => 'a',
	        'â' => 'a',
	        'ã' => 'a',
	        'ä' => 'a',
	        'å' => 'a',
	        'æ' => 'a',
	        'ç' => 'c',
	        'è' => 'e',
	        'é' => 'e',
	        'ê' => 'e',
	        'ë' => 'e',
	        'ì' => 'i',
	        'í' => 'i',
	        'î' => 'i',
	        'ï' => 'i',
	        'ð' => 'o',
	        'ñ' => 'n',
	        'ò' => 'o',
	        'ó' => 'o',
	        'ô' => 'o',
	        'õ' => 'o',
	        'ö' => 'o',
	        'ø' => 'o',
	        'ù' => 'u',
	        'ú' => 'u',
	        'û' => 'u',
	        'ý' => 'y',
	        'ý' => 'y',
	        'þ' => 'b',
	        'ÿ' => 'y',
	        'Ŕ' => 'R',
	        'ŕ' => 'r',
	        '/' => '-',
	        ' ' => '-',
	        '.' => '-',
	    );

	    $string = strtr($string, $list);
	    $string = preg_replace('/-{2,}/', '-', $string);
	    $string = strtolower($string);

	    return $string;
	}
}
