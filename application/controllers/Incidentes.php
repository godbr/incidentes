<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Incidentes extends CI_Controller {

	function __construct(){
		parent::__construct();		
		$this->load->model('Incidentes_model', 'incidentes');
		$this->load->model('Tipo_incidentes_model', 'tipo_incidentes');
	}

	public function index(){	
		$dados = array(
			'titulo_site' => 'Incidentes', 
			'current_page' => 'Incidentes', 
			'titulo' => 'Incidentes', 
			'titulo_page' => 'Incidentes',
			'url_add' => 'incidentes/add',
			'url_edit' => 'incidentes/edit',
			'url_delete' => 'incidentes/delete'
		);
		
		$dados['incidentes'] = $this->incidentes->list_all();

		$this->load->view('incidentes/index', $dados);	
	}

	public function add($id=Null){

		# Array de Dados de Texto
		$dados = array(
			'titulo_site' => 'Incidentes', 
			'previous_page' => 'Incidentes', 
			'url_previous' => base_url('incidentes'),
			'current_page' => 'Adicionar', 
			'titulo' => 'Incidentes', 
			'titulo_page' => 'Adicionar Incidentes',
			'url_add' => 'incidentes/add',
			'url_edit' => 'incidentes/edit/',
			'url_delete' => 'incidentes/delete/'
		);

		# Verifica se a ação é ADD ou EDIT
		if(!empty($id)){
			$dados['id'] = $id;
			$dados['current_page'] = "Editar";
			$dados['titulo_page'] = "Editar Incidente";
			$dados['fields'] = $this->incidentes->GetById($id);
		}

		# Verifica se é POST a ação
		if($this->input->post()){

			$this->form_validation->set_rules('titulo', 'Título', 'trim|required');
			$this->form_validation->set_rules('descricao', 'Descrição', 'trim|required');
			$this->form_validation->set_rules('tipo', 'Tipo', 'trim|required');

			# Faz a validação dos dados obrigatórios
			if($this->form_validation->run() == FALSE){

				$this->session->set_flashdata('error', validation_errors());
				if(!empty($id)){
					redirect('incidentes/add/'.$id, 'refresh');
				}else{
					redirect('incidentes/add', 'refresh');
				}
				

			}else{

				# Cria o Array para Inserção no Banco de Dados
				$inserir = array(
					'titulo' => $this->input->post("titulo"),
					'descricao' => $this->input->post("descricao"),
					'criticidade' => $this->input->post("criticidade"),
					'tipo_id' => $this->input->post("tipo")
				);

				if(!empty($id)){
					$inserir['status'] = $this->input->post("status");
				}

				if(!empty($id)){
					# Executa a Query de Atualização
					$status = $this->incidentes->Atualizar($id, $inserir);
				}else{
					# Executa a Query de Inserção
					$status = $this->incidentes->Inserir($inserir);
				}
				

				# Verifica se os dados foram adicionados
				if(!$status){
					$this->session->set_flashdata('error', 'Algo deu errado no cadastro!');		
				}else{
					if(!empty($id)){
						$this->session->set_flashdata('success', 'Incidente atualizado com sucesso!');
					}else{
						$this->session->set_flashdata('success', 'Incidente inserido com sucesso!');
					}					
					redirect('incidentes', 'refresh');	
				}
			}

		}			
		
		# Cria a foreikeign com Tipos cadastrados
		$tipo = $this->tipo_incidentes->get_all();
		$options = array('' => '');
		foreach($tipo as $res){
			$options[$res->id] = $res->nome;
			
		}	
		$dados['options'] = $options;

		# Select para o campo Criticidade
		$options_criticidade = array('baixo' => 'Baixo', 'médio' => 'Médio', 'alto' => 'Alto');
		$dados['options_criticidade'] = $options_criticidade;

		# Select para o campo Status
		$options_status = array('aberto' => 'Aberto', 'fechado' => 'Fechado');
		$dados['options_status'] = $options_status;

		$this->load->view('incidentes/add', $dados);
	}

	public function delete($id){

		$result = $this->incidentes->Excluir($id);
					
		if($result){
			$this->session->set_flashdata('success', 'Incidente deletado com sucesso!');
		}else{
			$this->session->set_flashdata('error', 'Algo deu errado!');
		}
		redirect('incidentes', 'refresh');
	}

	# END OF USER

}
