<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tipo_incidentes extends CI_Controller {

	function __construct(){
		parent::__construct();		
		$this->load->model('Incidentes_model', 'incidentes');
		$this->load->model('Tipo_incidentes_model', 'tipo_incidentes');
	}

	public function index(){	
		$dados = array(
			'titulo_site' => 'Tipo Incidentes', 
			'current_page' => 'Tipo Incidentes', 
			'titulo' => 'Tipo Incidentes', 
			'titulo_page' => 'Tipo Incidentes',
			'url_add' => 'tipo_incidentes/add',
			'url_edit' => 'tipo_incidentes/edit',
			'url_delete' => 'tipo_incidentes/delete'
		);
		
		$dados['incidentes'] = $this->tipo_incidentes->get_all();

		$this->load->view('tipo_incidentes/index', $dados);	
	}

	public function add($id=Null){

		# Array de Dados de Texto
		$dados = array(
			'titulo_site' => 'Tipo Incidentes', 
			'previous_page' => 'Tipo Incidentes', 
			'url_previous' => base_url('tipo_incidentes'),
			'current_page' => 'Adicionar', 
			'titulo' => 'Incidentes', 
			'titulo_page' => 'Adicionar Tipo Incidentes',
			'url_add' => 'tipo_incidentes/add/',
			'url_edit' => 'tipo_incidentes/edit/',
			'url_delete' => 'tipo_incidentes/delete/'
		);

		# Verifica se a ação é ADD ou EDIT
		if(!empty($id)){
			$dados['id'] = $id;
			$dados['current_page'] = "Editar";
			$dados['titulo_page'] = "Editar Tipo Incidente";
			$dados['fields'] = $this->tipo_incidentes->GetById($id);
		}

		# Verifica se é POST a ação
		if($this->input->post()){

			$this->form_validation->set_rules('nome', 'Nome', 'trim|required');

			# Faz a validação dos dados obrigatórios
			if($this->form_validation->run() == FALSE){

				$this->session->set_flashdata('error', validation_errors());
				if(!empty($id)){
					redirect('tipo_incidentes/add/'.$id, 'refresh');
				}else{
					redirect('tipo_incidentes/add', 'refresh');
				}
				

			}else{

				# Cria o Array para Inserção no Banco de Dados
				$inserir = array(
					'nome' => $this->input->post("nome")
				);

				if(!empty($id)){
					# Executa a Query de Atualização
					$status = $this->tipo_incidentes->Atualizar($id, $inserir);
				}else{
					# Executa a Query de Inserção
					$status = $this->tipo_incidentes->Inserir($inserir);
				}
				

				# Verifica se os dados foram adicionados
				if(!$status){
					$this->session->set_flashdata('error', 'Algo deu errado no cadastro!');		
				}else{
					if(!empty($id)){
						$this->session->set_flashdata('success', 'Tipo Incidente atualizado com sucesso!');
					}else{
						$this->session->set_flashdata('success', 'Tipo Incidente inserido com sucesso!');
					}					
					redirect('tipo_incidentes', 'refresh');	
				}
			}

		}			

		$this->load->view('tipo_incidentes/add', $dados);
	}

	public function delete($id){

		$result = $this->incidentes->ExcluirCascade($id);
		$result = $this->tipo_incidentes->Excluir($id);
					
		if($result){
			$this->session->set_flashdata('success', 'Incidente deletado com sucesso!');
		}else{
			$this->session->set_flashdata('error', 'Algo deu errado!');
		}
		redirect('tipo_incidentes', 'refresh');
	}

	# END OF USER

}
