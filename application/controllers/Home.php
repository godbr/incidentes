<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	function __construct(){
		parent::__construct();
	}

	public function index(){	
		$dados = array(
			'titulo_site' => 'Incidentes', 
			'current_page' => 'Incidentes', 
			'titulo' => 'Incidentes', 
			'titulo_page' => 'Incidentes'
		);
		$this->load->view('home', $dados);	
	}

	# BEGIN USERS
	public function usuarios(){
		$this->load->model("Incidentes_model", 'incidentes', True);
		$dados = array(
			'titulo_site' => 'Incidentes', 
			'current_page' => 'Incidentes', 
			'titulo' => 'Incidentes', 
			'titulo_page' => 'Incidentes',
			'url_add' => 'usuarios/add',
			'url_edit' => 'usuarios/edit/',
			'url_delete' => 'usuarios/delete/'
		);
		$dados['user'] = $this->user->get_all();
		$this->load->view('usuarios/usuarios', $dados);
	}

	public function user_add(){

		if($this->input->post()){

			$this->load->model('user_model', 'user');
			$this->form_validation->set_rules('nome', 'Nome', 'trim|required');
			$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[user.email]');
			$this->form_validation->set_rules('empresa', 'Empresa', 'trim|required');
			$this->form_validation->set_rules('senha', 'Senha', 'trim|required');
			$this->form_validation->set_rules('senha2', 'Confirmar Senha', 'trim|required|matches[senha]');

			if($this->form_validation->run() == FALSE){

				$this->session->set_flashdata('error', validation_errors());
				redirect('usuarios/add', 'refresh');

			}else{

				$inserir = array(
					'nome' => $this->input->post("nome"),
					'email' => $this->input->post("email"),
					'senha' => md5($this->input->post("senha")),
					'empresa_id' => $this->input->post("empresa"),
					'role' => $this->input->post("permissao")
				);

				$status = $this->user->Inserir($inserir);

				if(!$status){
					$this->session->set_flashdata('error', 'Algo Errado não está certo!');		
				}else{
					$this->session->set_flashdata('success', 'Usuário inserido com sucesso!');
					redirect('usuarios', 'refresh');	
				}
			}

		}	
		$dados = array(
			'titulo_site' => 'Guru Astral', 
			'previous_page' => 'Usuários', 
			'url_previous' => base_url('usuarios'),
			'current_page' => 'Adicionar', 
			'titulo' => 'Usuários', 
			'titulo_page' => 'Adicionar Usuários',
			'url_add' => 'usuarios/add',
			'url_edit' => 'usuarios/edit/',
			'url_delete' => 'usuarios/delete/'
		);			

		$this->load->model('empresa_model', 'model');
		$empresas = $this->model->GetAll('nome','asc');
		$options = array('' => '');
		foreach($empresas as $res){
			$options[$res->id] = $res->nome;
			
		}	
		$dados['options'] = $options;	


		$this->load->view('usuarios/add-usuario', $dados);
	}

	public function user_edit($id){
		$this->load->model('user_model', 'user');

		if($this->input->post()){
			
			$this->form_validation->set_rules('nome', 'Nome', 'trim|required');
			$this->form_validation->set_rules('empresa', 'Empresa', 'trim|required');
			if(!empty($this->input->post("senha"))){
				$this->form_validation->set_rules('senha', 'Senha', 'trim|required');
				$this->form_validation->set_rules('senha2', 'Confirmar Senha', 'trim|required|matches[senha]');
			}

			if($this->form_validation->run() == FALSE){

				$this->session->set_flashdata('error', validation_errors());
				redirect('usuarios/edit/'.$id, 'refresh');

			}else{

				$inserir = array(
					'nome' => $this->input->post("nome"),
					'empresa_id' => $this->input->post("empresa"),
					'role' => $this->input->post("permissao")
				);

				if(!empty($this->input->post("senha"))){
					$inserir['senha'] = md5($this->input->post("senha")); 					
				}

				$status = $this->user->Atualizar($id, $inserir);

				if(!$status){
					$this->session->set_flashdata('error', 'Algo Errado não está certo!');		
				}else{
					$this->session->set_flashdata('success', 'Usuário atualizado com sucesso!');
					redirect('usuarios', 'refresh');	
				}
			}

		}	
		$dados = array(
			'titulo_site' => 'Guru Astral', 
			'previous_page' => 'Usuários', 
			'url_previous' => base_url('usuarios'),
			'current_page' => 'Editar', 
			'titulo' => 'Usuários', 
			'titulo_page' => 'Editar Usuários',
			'url_add' => 'usuarios/add',
			'url_edit' => 'usuarios/edit/',
			'url_delete' => 'usuarios/delete/'
		);	
		$dados['fields'] = $this->user->GetById($id);

		if(empty($dados['fields'])){
			$this->session->set_flashdata('error', 'Algo Errado não está certo!');
			redirect('usuarios', 'refresh');	
		}
		$this->load->model('empresa_model', 'model');
		$empresas = $this->model->get_all();
		$options = array('' => '');
		foreach($empresas as $res){
			$options[$res->id] = $res->nome;
			
		}	
		$dados['options'] = $options;	
		$this->load->view('usuarios/edit-usuario', $dados);
	}

	public function user_delete($id){
		$this->load->model('user_model', 'user');

		$result = $this->user->Excluir($id);
					
		if($result){
			$this->session->set_flashdata('success', 'Usuário deletado com sucesso!');
			redirect('usuarios', 'refresh');
		}else{
			$this->session->set_flashdata('error', 'Algo Errado não está certo!');
			redirect('usuarios', 'refresh');
		}
	}

	# END OF USER

	# BEGIN MODALIDADES
	public function modalidades(){
		$this->load->model("modalidades_model", 'modal', True);
		$dados = array(
			'titulo_site' => 'Guru Astral', 
			'current_page' => 'Modalidades', 
			'titulo' => 'Modalidades', 
			'titulo_page' => 'Modalidades',
			'url_add' => 'modalidades/add',
			'url_edit' => 'modalidades/edit/',
			'url_delete' => 'modalidades/delete/'
		);
		$dados['user'] = $this->modal->GetAll();
		$this->load->view('modalidades', $dados);
	}

	public function modalidades_add(){

		if($this->input->post()){

			$this->load->library('upload');

			$this->load->model('modalidades_model', 'modal');
			$this->form_validation->set_rules('nome', 'Nome', 'trim|required');

			if($this->form_validation->run() == FALSE){

				$this->session->set_flashdata('error', validation_errors());
				redirect('modalidades/add', 'refresh');

			}else{

				$inserir = array(
					'nome' => $this->input->post("nome")
				);

				$status = $this->modal->Inserir($inserir);
				$id = $this->modal->last_id();

				# Upload do Edital

				if(!empty($_FILES['imagem']['name'])){

					$arquivo = strtolower(str_replace(" ", "", date("YmdHis").$_FILES['imagem']['name']));
					$path = './../modal/';
					$config['upload_path'] = $path;
					$config['allowed_types'] = '*';
					$config['max_size'] = 10000;
					$config['file_name'] = $arquivo;
				   
					if (!is_dir($path)) {
						mkdir($path, 0777, $recursive = true);
					}

					$this->upload->initialize($config);

					if (!$this->upload->do_upload('imagem')){
						$this->session->set_flashdata('error', $this->upload->display_errors());
						redirect('modalidades/add', 'refresh');
					}
					else{	                	
						$update = array('imagem' => $arquivo);
						$this->modal->Atualizar($id, $update);
					}

				}

				if(!$status){
					$this->session->set_flashdata('error', 'Algo Errado não está certo!');		
				}else{
					$this->session->set_flashdata('success', 'Modalidade inserida com sucesso!');
					redirect('modalidades', 'refresh');	
				}
			}

		}	
		$dados = array(
			'titulo_site' => 'Guru Astral', 
			'previous_page' => 'Modalidades', 
			'url_previous' => base_url('modalidades'),
			'current_page' => 'Adicionar', 
			'titulo' => 'Modalidades', 
			'titulo_page' => 'Adicionar Modalidade',
			'url_add' => 'modalidades/add',
			'url_edit' => 'modalidades/edit/',
			'url_delete' => 'modalidades/delete/'
		);			
		$this->load->view('add-modalidade', $dados);
	}

	public function modalidades_edit($id){
		$this->load->model('modalidades_model', 'modal');

		if(!empty($_GET['imagem'])){
			@unlink(base64_decode($_GET['imagem']));
			$update_edital = array('imagem' => '');
			$s = $this->modal->Atualizar($id, $update_edital);
			if($s){
				$this->session->set_flashdata('success', 'Imagem removida.');
				redirect('modalidades/edit/'.$id, 'refresh');
			}
		}

		if($this->input->post()){

			$this->load->library('upload');
			
			$this->form_validation->set_rules('nome', 'Nome', 'trim|required');

			if($this->form_validation->run() == FALSE){

				$this->session->set_flashdata('error', validation_errors());
				redirect('modalidades/edit/'.$id, 'refresh');

			}else{

				$inserir = array(
					'nome' => $this->input->post("nome"),
				);

				$status = $this->modal->Atualizar($id, $inserir);

				if(!empty($_FILES['imagem']['name'])){

					$arquivo = strtolower(str_replace(" ", "", date("YmdHis").$_FILES['imagem']['name']));
					$path = './../modal/';
					$config['upload_path'] = $path;
					$config['allowed_types'] = '*';
					$config['max_size'] = 10000;
					$config['file_name'] = $arquivo;
				   
					if (!is_dir($path)) {
						mkdir($path, 0777, $recursive = true);
					}

					$this->upload->initialize($config);

					if (!$this->upload->do_upload('imagem')){
						$this->session->set_flashdata('error', $this->upload->display_errors());
						redirect('modalidades/edit/'.$id, 'refresh');
					}
					else{	                	
						$update = array('imagem' => $arquivo);
						$this->modal->Atualizar($id, $update);
					}

				}

				if(!$status){
					$this->session->set_flashdata('error', 'Algo Errado não está certo!');		
				}else{
					$this->session->set_flashdata('success', 'Modalidade atualizada com sucesso!');
					redirect('modalidades', 'refresh');	
				}
			}

		}	
		$dados = array(
			'titulo_site' => 'Guru Astral', 
			'previous_page' => 'Modalidades', 
			'url_previous' => base_url('modalidades'),
			'current_page' => 'Editar', 
			'titulo' => 'Modalidades', 
			'titulo_page' => 'Editar Modalidades',
			'url_add' => 'modalidades/add',
			'url_edit' => 'modalidades/edit/',
			'url_delete' => 'modalidades/delete/'
		);	
		$dados['fields'] = $this->modal->GetById($id);

		if(empty($dados['fields'])){
			$this->session->set_flashdata('error', 'Algo Errado não está certo!');
			redirect('modalidades', 'refresh');	
		}

		$this->load->view('edit-modalidade', $dados);
	}

	public function modalidades_delete($id){
		$this->load->model('modalidades_model', 'modal');

		$result = $this->modal->Excluir($id);
					
		if($result){
			$this->session->set_flashdata('success', 'Modalidade deletada com sucesso!');
			redirect('modalidades', 'refresh');
		}else{
			$this->session->set_flashdata('error', 'Algo Errado não está certo!');
			redirect('modalidades', 'refresh');
		}
	}

	#END OF MODALIDADES

	# BEGIN LICITACAO
	public function licitacao(){
		$this->load->model("licitacao_model", 'licitacao', True);
		$dados = array(
			'titulo_site' => 'Guru Astral', 
			'current_page' => 'Licitação', 
			'titulo' => 'Licitação', 
			'titulo_page' => 'Licitação',
			'url_add' => 'licitacao/add',
			'url_edit' => 'licitacao/edit/',
			'url_delete' => 'licitacao/delete/'
		);
		$dados['user'] = $this->licitacao->get_all();
		$this->load->view('licitacao', $dados);
	}

	public function licitacao_add(){

		if($this->input->post()){

			$this->load->library('upload');
			$this->load->model('licitacao_model', 'dados');

			$this->form_validation->set_rules('inicio', 'Início', 'trim|required');
			$this->form_validation->set_rules('final', 'Final', 'trim|required');
			$this->form_validation->set_rules('modalidades', 'Modalidades', 'trim|required');
			$this->form_validation->set_rules('titulo', 'Titulo', 'trim|required');
			$this->form_validation->set_rules('texto', 'Texto', 'trim|required');

			if($this->form_validation->run() == FALSE){

				$this->session->set_flashdata('error', validation_errors());
				redirect('licitacao/add', 'refresh');

			}else{

				$inserir = array(
					'modalidade_id' => $this->input->post("modalidades"),
					'inicio' => $this->input->post("inicio"),
					'final' => $this->input->post("final"),
					'titulo' => $this->input->post("titulo"),
					'texto' => $this->input->post("texto"),
					'download' => $this->input->post("download")
				);

				$status = $this->dados->Inserir($inserir);
				$id = $this->dados->last_id();

				# Upload do Edital

				if(!empty($_FILES['edital']['name'])){

					$arquivo = strtolower(str_replace(" ", "", date("YmdHis").$_FILES['edital']['name']));
					$path = './../edital/';
					$config['upload_path'] = $path;
					$config['allowed_types'] = '*';
					$config['max_size'] = 10000;
					$config['file_name'] = $arquivo;
				   
					if (!is_dir($path)) {
						mkdir($path, 0777, $recursive = true);
					}

					$this->upload->initialize($config);

					if (!$this->upload->do_upload('edital')){
						$this->session->set_flashdata('error', $this->upload->display_errors());
						redirect('licitacao/add', 'refresh');
					}
					else{	                	
						$update = array('edital' => $arquivo);
						$this->dados->Atualizar($id, $update);
					}

				}

				# Multiple Upload Anexos
				$total = count($_FILES['anexo']['name']);

				# Verifica se existem arquivos selecionados
				if($total > 0){

					for($i=0; $i<$total; $i++){    

						if(!empty($_FILES['anexo']['name'][$i])){ 

							$arquivo = strtolower(str_replace(" ", "", date("YmdHis").$_FILES['anexo']['name'][$i]));
							$path = './../files/';
							$config['upload_path'] = $path;
							$config['allowed_types'] = '*';
							$config['max_size'] = 10000;
							$config['file_name'] = $arquivo;
						   
							if (!is_dir($path)) {
								mkdir($path, 0777, $recursive = true);
							}

							$_FILES['userFile']['name'] = $arquivo;
							$_FILES['userFile']['type'] = $_FILES['anexo']['type'][$i];
							$_FILES['userFile']['tmp_name'] = $_FILES['anexo']['tmp_name'][$i];
							$_FILES['userFile']['error'] = $_FILES['anexo']['error'][$i];
							$_FILES['userFile']['size'] = $_FILES['anexo']['size'][$i];
							
							$this->upload->initialize($config);

							if (!$this->upload->do_upload('userFile')){
								$this->session->set_flashdata('error', $this->upload->display_errors());
								redirect('licitacao/add'.$redir, 'refresh');
							}     

							# Inicializa Model Mídia
							$this->load->model("midia_model", "midia");
							$midiaon = array(
								'licitacao_id' => $id,
								'arquivo' => $arquivo
							);
							$s = $this->midia->Inserir($midiaon);

							if(!$s){
								$this->session->set_flashdata('error', 'Erro Inserindo Mídia');
								redirect('licitacao/add'.$redir, 'refresh');    	
							}

						}
					}

				}

				
				
			}


			if(!$status){
				$this->session->set_flashdata('error', 'Algo Errado não está certo!');		
			}else{
				$this->session->set_flashdata('success', 'Licitação inserido com sucesso!');
				redirect('licitacao', 'refresh');	
			}
			

		}	
		$dados = array(
			'titulo_site' => 'Guru Astral', 
			'previous_page' => 'Licitação', 
			'url_previous' => base_url('licitacao'),
			'current_page' => 'Adicionar', 
			'titulo' => 'Licitação', 
			'titulo_page' => 'Adicionar Licitação',
			'url_add' => 'licitacao/add',
			'url_edit' => 'licitacao/edit/',
			'url_delete' => 'licitacao/delete/'
		);
		$this->load->model('modalidades_model', 'model');
		$modalidades = $this->model->GetAll('nome','asc');
		$options = array('' => '');
		foreach($modalidades as $res){
			$options[$res->id] = $res->nome;
			
		}	
		$dados['options'] = $options;	
		$this->load->view('add-licitacao', $dados);
	}

	public function licitacao_edit($id){

		$this->load->model("midia_model", "midia");
		$this->load->model('licitacao_model', 'dados');

		if(!empty($_GET['edital'])){
			@unlink(base64_decode($_GET['edital']));
			$update_edital = array('edital' => '');
			$s = $this->dados->Atualizar($id, $update_edital);
			if($s){
				$this->session->set_flashdata('success', 'Arquivo de Edital removido com sucesso.');
				redirect('licitacao/edit/'.$id, 'refresh');
			}
		}

		if(!empty($_GET['midia_id']) && !empty($_GET['anexo'])){
			@unlink(base64_decode($_GET['anexo']));
			$s = $this->midia->Excluir($_GET['midia_id']);	
			if($s){
				$this->session->set_flashdata('success', 'Arquivo de Anexo removido com sucesso.');
				redirect('licitacao/edit/'.$id, 'refresh');
			}
		}

		if($this->input->post()){

			$this->load->library('upload');			

			$this->form_validation->set_rules('inicio', 'Início', 'trim|required');
			$this->form_validation->set_rules('final', 'Final', 'trim|required');
			$this->form_validation->set_rules('modalidades', 'Modalidades', 'trim|required');
			$this->form_validation->set_rules('titulo', 'Titulo', 'trim|required');
			$this->form_validation->set_rules('texto', 'Texto', 'trim|required');

			if($this->form_validation->run() == FALSE){

				$this->session->set_flashdata('error', validation_errors());
				redirect('licitacao/edit/'.$id, 'refresh');

			}else{

				$inserir = array(
					'modalidade_id' => $this->input->post("modalidades"),
					'inicio' => $this->input->post("inicio"),
					'final' => $this->input->post("final"),
					'titulo' => $this->input->post("titulo"),
					'texto' => $this->input->post("texto"),
					'download' => $this->input->post("download")
				);

				$status = $this->dados->Atualizar($id, $inserir);

				# Upload do Edital
				if(!empty($_FILES['edital']['name'])){

					$arquivo = strtolower(str_replace(" ", "", date("YmdHis").$_FILES['edital']['name']));
					$path = './../edital/';
					$config['upload_path'] = $path;
					$config['allowed_types'] = '*';
					$config['max_size'] = 10000;
					$config['file_name'] = $arquivo;
				   
					if (!is_dir($path)) {
						mkdir($path, 0777, $recursive = true);
					}

					$this->upload->initialize($config);

					if (!$this->upload->do_upload('edital')){
						$this->session->set_flashdata('error', $this->upload->display_errors().' em EDITAL');
						redirect('licitacao/edit/'.$id, 'refresh');
					}
					else{	                	
						$update = array('edital' => $arquivo);
						$this->dados->Atualizar($id, $update);
					}

				}

				# Multiple Upload Anexos
				$total = count($_FILES['anexo']['name']);

				# Verifica se existem arquivos selecionados
				if($total > 0){

					for($i=0; $i<$total; $i++){ 

						if(!empty($_FILES['anexo']['name'][$i])){  

							$arquivo = strtolower(str_replace(" ", "", date("YmdHis").$_FILES['anexo']['name'][$i]));
							$path = './../files/';
							$config['upload_path'] = $path;
							$config['allowed_types'] = '*';
							$config['max_size'] = 10000;
							$config['file_name'] = $arquivo;
						   
							if (!is_dir($path)) {
								mkdir($path, 0777, $recursive = true);
							}

							$_FILES['userFile']['name'] = $arquivo;
							$_FILES['userFile']['type'] = $_FILES['anexo']['type'][$i];
							$_FILES['userFile']['tmp_name'] = $_FILES['anexo']['tmp_name'][$i];
							$_FILES['userFile']['error'] = $_FILES['anexo']['error'][$i];
							$_FILES['userFile']['size'] = $_FILES['anexo']['size'][$i];
							
							$this->upload->initialize($config);

							if (!$this->upload->do_upload('userFile')){
								$this->session->set_flashdata('error', $this->upload->display_errors().' em ANEXOS');
								redirect('licitacao/edit/'.$id, 'refresh');
							}		                
							$midiaon = array(
								'licitacao_id' => $id,
								'arquivo' => $arquivo
							);
							$s = $this->midia->Inserir($midiaon);

							if(!$s){
								$this->session->set_flashdata('error', 'Erro Inserindo Mídia');
								redirect('licitacao/edit/'.$id, 'refresh');    	
							}
						}
					}

				}				
				
			}


			if(!$status){
				$this->session->set_flashdata('error', 'Algo Errado não está certo!');		
			}else{
				$this->session->set_flashdata('success', 'Licitação Atualizada com sucesso!');
				redirect('licitacao', 'refresh');	
			}
			

		}	
		$dados = array(
			'titulo_site' => 'Guru Astral', 
			'previous_page' => 'Licitação', 
			'url_previous' => base_url('licitacao'),
			'current_page' => 'Editar', 
			'titulo' => 'Licitação', 
			'titulo_page' => 'Editar Licitação',
			'url_add' => 'licitacao/add',
			'url_edit' => 'licitacao/edit/',
			'url_delete' => 'licitacao/delete/'
		);
		$this->load->model('modalidades_model', 'model');
		$modalidades = $this->model->GetAll('nome','asc');
		$options = array('' => '');
		foreach($modalidades as $res){
			$options[$res->id] = $res->nome;
			
		}	
		$dados['options'] = $options;	
		
		# Select Midia
		$dados['midia'] = $this->midia->get_midia_id($id);
		$dados['licitacao'] = $this->dados->get_id($id);		

		$this->load->view('edit-licitacao', $dados);
	}

	public function licitacao_delete($id){
		$this->load->model('licitacao_model', 'dados');

		$result = $this->dados->Excluir($id);
					
		if($result){
			$this->session->set_flashdata('success', 'Licitação deletada com sucesso!');
			redirect('licitacao', 'refresh');
		}else{
			$this->session->set_flashdata('error', 'Algo Errado não está certo!');
			redirect('licitacao', 'refresh');
		}
	}

	public function upload(){

		//carrega a biblioteca de upload do CI
		$this->load->library('upload');

		//define o caminho para salvar as imagens
		$path = './files';

		//Configuração do upload
		//informa o diretorio para salvar as imagens
		$config['upload_path'] = $path;

		//define os tipos de arquivos suportados
		$config['allowed_types'] = 'gif|jpg|jpeg|png|pdf|xlsx|xls|csv|doc|docx';

		//define o tamanho máximo do arquivo (em Kb)
		$config['max_size'] = '5000';

		//verifica se o path é válido, se não for cria o diretório "uploads"
		if (!is_dir($path)) {
			mkdir($path, 0777, $recursive = true);
		}
		//Inicializa o método de upload
		$this->upload->initialize($config);
		//processa o upload e verifica o status
		if (!$this->upload->do_upload('file')) {
			//Determina o status do header
			$this->output->set_status_header('400');
			//Retorna a mensagem de erro a ser exibida
			echo $this->upload->display_errors(null,null);
		} else {
			//Determina o status do header
			$this->output->set_status_header('200');
		}
	}

	# END OF LICITACAO

	# BEGIN RELATORIOS
	public function relatorios(){
		$this->load->model("relatorios_model", 'relatorios', True);
		$dados = array(
			'titulo_site' => 'Guru Astral', 
			'current_page' => 'Relatórios', 
			'titulo' => 'Relatórios', 
			'titulo_page' => 'Relatórios',
			'url_add' => 'relatorios/add',
			'url_edit' => 'relatorios/edit/',
			'url_delete' => 'relatorios/delete/'
		);
		if(!empty($_GET['l_id'])){
			$licitacao_id = intval($_GET['l_id']);
		}
		$dados['user'] = $this->relatorios->get_all($licitacao_id);
		$this->load->view('relatorios', $dados);
	}
	# END RELATORIOS

	# BEGIN RELATORIOS
	public function lrelatorios(){
		$this->load->model("relatorios_model", 'relatorios', True);
		$dados = array(
			'titulo_site' => 'Guru Astral', 
			'current_page' => 'Relatórios', 
			'titulo' => 'Relatórios', 
			'titulo_page' => 'Relatórios',
			'url_add' => 'relatorios/add',
			'url_edit' => 'relatorios/edit/',
			'url_delete' => 'relatorios/delete/'
		);
		$dados['user'] = $this->relatorios->get_list();
		$this->load->view('lrelatorios', $dados);
	}
	# END RELATORIOS

	# BEGIN MODALIDADES
	public function licitantes(){
		$this->load->model("licitantes_model", 'licitantes', True);
		$dados = array(
			'titulo_site' => 'Guru Astral', 
			'current_page' => 'Licitantes', 
			'titulo' => 'Licitantes', 
			'titulo_page' => 'Licitantes',
			'url_add' => 'licitantes/add',
			'url_edit' => 'licitantes/edit/',
			'url_delete' => 'licitantes/delete/'
		);
		$dados['user'] = $this->licitantes->GetAll();
		$this->load->view('licitantes', $dados);
	}

	public function licitantes_add(){

		if($this->input->post()){

			$this->load->model('licitantes_model', 'licitantes');
			$this->form_validation->set_rules('nome', 'Nome', 'trim|required');
			$this->form_validation->set_rules('tipo', 'Tipo', 'trim|required');

			if($this->input->post("tipo") == 'fisico'){
				$this->form_validation->set_rules('cpf', 'CPF', 'trim|required');
			}else{
				$this->form_validation->set_rules('cnpj', 'CNPJ', 'trim|required');
			}
			$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
			$this->form_validation->set_rules('endereco', 'Endereço', 'trim|required');
			$this->form_validation->set_rules('cep', 'Cep', 'trim|required');
			$this->form_validation->set_rules('cidade', 'Cidade', 'trim|required');
			$this->form_validation->set_rules('estado', 'Estado', 'trim|required');
			$this->form_validation->set_rules('telefone', 'Telefone', 'trim|required');

			if($this->form_validation->run() == FALSE){

				$this->session->set_flashdata('error', validation_errors());
				redirect('licitantes/add', 'refresh');

			}else{

				$inserir = array(
					'data' => date('Y-m-d'),
					'nome' => $this->input->post("nome"),
					'tipo' => $this->input->post("tipo"),
					'cpf' => $this->input->post("cpf"),
					'cnpj' => $this->input->post("cnpj"),
					'email' => $this->input->post("email"),
					'endereco' => $this->input->post("endereco"),
					'cep' => $this->input->post("cep"),
					'cidade' => $this->input->post("cidade"),
					'estado' => $this->input->post("estado"),
					'telefone' => $this->input->post("telefone"),
					'fax' => $this->input->post("fax")
				);

				$status = $this->licitantes->Inserir($inserir);

				if(!$status){
					$this->session->set_flashdata('error', 'Algo Errado não está certo!');		
				}else{
					$this->session->set_flashdata('success', 'Licitante inserido com sucesso!');
					redirect('licitantes', 'refresh');	
				}
			}

		}	
		$dados = array(
			'titulo_site' => 'Guru Astral', 
			'previous_page' => 'Licitantes', 
			'url_previous' => base_url('licitantes'),
			'current_page' => 'Adicionar', 
			'titulo' => 'Licitantes', 
			'titulo_page' => 'Adicionar Licitante',
			'url_add' => 'licitantes/add',
			'url_edit' => 'licitantes/edit/',
			'url_delete' => 'licitantes/delete/'
		);
		$dados['options'] = array('' => '', 'fisico' => 'Pessoa Física', 'juridico' => 'Pessoa Jurídica');
		$dados['estado'] = array(
			'' => '',
			'AC'=>'Acre',
			'AL'=>'Alagoas',
			'AP'=>'Amapá',
			'AM'=>'Amazonas',
			'BA'=>'Bahia',
			'CE'=>'Ceará',
			'DF'=>'Distrito Federal',
			'ES'=>'Espírito Santo',
			'GO'=>'Goiás',
			'MA'=>'Maranhão',
			'MT'=>'Mato Grosso',
			'MS'=>'Mato Grosso do Sul',
			'MG'=>'Minas Gerais',
			'PA'=>'Pará',
			'PB'=>'Paraíba',
			'PR'=>'Paraná',
			'PE'=>'Pernambuco',
			'PI'=>'Piauí',
			'RJ'=>'Rio de Janeiro',
			'RN'=>'Rio Grande do Norte',
			'RS'=>'Rio Grande do Sul',
			'RO'=>'Rondônia',
			'RR'=>'Roraima',
			'SC'=>'Santa Catarina',
			'SP'=>'São Paulo',
			'SE'=>'Sergipe',
			'TO'=>'Tocantins'
		);			
		$this->load->view('add-licitante', $dados);
	}

	public function licitantes_edit($id){

		$this->load->model('licitantes_model', 'licitantes');
		
		if($this->input->post()){			
			
			$this->form_validation->set_rules('nome', 'Nome', 'trim|required');
			$this->form_validation->set_rules('tipo', 'Tipo', 'trim|required');

			if($this->input->post("tipo") == 'fisico'){
				$this->form_validation->set_rules('cpf', 'CPF', 'trim|required');
			}else{
				$this->form_validation->set_rules('cnpj', 'CNPJ', 'trim|required');
			}
			$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
			$this->form_validation->set_rules('endereco', 'Endereço', 'trim|required');
			$this->form_validation->set_rules('cep', 'Cep', 'trim|required');
			$this->form_validation->set_rules('cidade', 'Cidade', 'trim|required');
			$this->form_validation->set_rules('estado', 'Estado', 'trim|required');
			$this->form_validation->set_rules('telefone', 'Telefone', 'trim|required');

			if($this->form_validation->run() == FALSE){

				$this->session->set_flashdata('error', validation_errors());
				redirect('licitantes/edit', 'refresh');

			}else{

				$inserir = array(
					'nome' => $this->input->post("nome"),
					'tipo' => $this->input->post("tipo"),
					'cpf' => $this->input->post("cpf"),
					'cnpj' => $this->input->post("cnpj"),
					'email' => $this->input->post("email"),
					'endereco' => $this->input->post("endereco"),
					'cep' => $this->input->post("cep"),
					'cidade' => $this->input->post("cidade"),
					'estado' => $this->input->post("estado"),
					'telefone' => $this->input->post("telefone"),
					'fax' => $this->input->post("fax")
				);

				$status = $this->licitantes->Atualizar($id, $inserir);

				if(!$status){
					$this->session->set_flashdata('error', 'Algo Errado não está certo!');		
				}else{
					$this->session->set_flashdata('success', 'Licitante atualizado com sucesso!');
					redirect('licitantes', 'refresh');	
				}
			}

		}	
		$dados = array(
			'titulo_site' => 'Guru Astral', 
			'previous_page' => 'Licitantes', 
			'url_previous' => base_url('licitantes'),
			'current_page' => 'Editar', 
			'titulo' => 'Licitantes', 
			'titulo_page' => 'Editar Licitante',
			'url_add' => 'licitantes/add',
			'url_edit' => 'licitantes/edit/',
			'url_delete' => 'licitantes/delete/'
		);
		$dados['options'] = array('' => '', 'fisico' => 'Pessoa Física', 'juridico' => 'Pessoa Jurídica');
		$dados['estado'] = array(
			'' => '',
			'AC'=>'Acre',
			'AL'=>'Alagoas',
			'AP'=>'Amapá',
			'AM'=>'Amazonas',
			'BA'=>'Bahia',
			'CE'=>'Ceará',
			'DF'=>'Distrito Federal',
			'ES'=>'Espírito Santo',
			'GO'=>'Goiás',
			'MA'=>'Maranhão',
			'MT'=>'Mato Grosso',
			'MS'=>'Mato Grosso do Sul',
			'MG'=>'Minas Gerais',
			'PA'=>'Pará',
			'PB'=>'Paraíba',
			'PR'=>'Paraná',
			'PE'=>'Pernambuco',
			'PI'=>'Piauí',
			'RJ'=>'Rio de Janeiro',
			'RN'=>'Rio Grande do Norte',
			'RS'=>'Rio Grande do Sul',
			'RO'=>'Rondônia',
			'RR'=>'Roraima',
			'SC'=>'Santa Catarina',
			'SP'=>'São Paulo',
			'SE'=>'Sergipe',
			'TO'=>'Tocantins'
		);			

		$dados['result'] = $this->licitantes->get_id($id);

		$this->load->view('edit-licitante', $dados);
	}

	public function licitantes_delete($id){
		$this->load->model('licitantes_model', 'licitantes');

		$result = $this->licitantes->Excluir($id);
					
		if($result){
			$this->session->set_flashdata('success', 'Licitante deletado com sucesso!');
			redirect('licitantes', 'refresh');
		}else{
			$this->session->set_flashdata('error', 'Algo Errado não está certo!');
			redirect('licitantes', 'refresh');
		}
	}

	#END OF MODALIDADES
}
