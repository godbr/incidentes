<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="pt-br" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="pt-br" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="pt-br">
    <!--<![endif]-->
    <?php $this->load->view('header'); ?>

    <body class="page-container-bg-solid page-header-menu-fixed">
        <div class="page-wrapper">
            <div class="page-wrapper-row">
                <div class="page-wrapper-top">
                    
					<!-- BEGIN HEAD -->
                     <?php $this->load->view('head'); ?>
					<!-- END HEAD -->
					
                </div>
            </div>
            <div class="page-wrapper-row full-height">
                <div class="page-wrapper-middle">
				
                    <!-- BEGIN CONTAINER -->
                    <div class="page-container">
                        <!-- BEGIN CONTENT -->
                        <div class="page-content-wrapper">
                            <!-- BEGIN CONTENT BODY -->
                            <!-- BEGIN PAGE HEAD-->
                            <div class="page-head">
                                <div class="container-fluid">
                                    <!-- BEGIN PAGE TITLE -->
                                    <div class="page-title">
                                        <h1><?php echo $titulo_page; ?></h1>
                                    </div>
                                    <!-- END PAGE TITLE -->
                                </div>
                            </div>
                            <!-- END PAGE HEAD-->
                            <!-- BEGIN PAGE CONTENT BODY -->
                            <div class="page-content">

                                

                                <div class="container-fluid">
                                   
								   <!-- BEGIN PAGE BREADCRUMBS -->
                                    <ul class="page-breadcrumb breadcrumb">
                                        <li>
                                            <a href="<?php echo base_url(); ?>">Home</a>
                                            <i class="fa fa-circle"></i>
                                        </li>
                                        <?php if(!empty($previous_page)) { ?>
                                        <li>
                                            <a href="<?php echo $url_previous; ?>"><?php echo $previous_page; ?></a>
                                            <i class="fa fa-circle"></i>
                                        </li>                 
                                        <?php } ?>
                                        <li>
                                            <span><?php echo $current_page; ?></span>
                                        </li>
                                    </ul>
                                    <!-- END PAGE BREADCRUMBS -->
									
									
                                    <!-- BEGIN PAGE CONTENT INNER -->
                                    <div class="page-content-inner">
                                        
                                        <div class="row">
                                            <div class="col-md-12">

                                                <div class="portlet light bordered">
                                                    <?php $this->load->view('messages'); ?>
                                                    <div class="portlet-body form">
                                                        <!-- BEGIN FORM-->
                                                        <?php echo form_open_multipart('', 'class="form-horizontal" id="formuser"'); ?>
                                                            <div class="form-body">

                                                                <div class="form-group">
                                                                    <label class="col-md-3 control-label">Nome</label>
                                                                    <div class="col-md-4">
                                                                        <?php echo form_input(array("type" => "text", "placeholder" => "Nome", "autocomplete" => "off", "name" => "nome", "id" => "nome", "class" => "form-control", "value" => $fields['nome'])); ?>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="col-md-3 control-label">Cor Section 1</label>
                                                                    <div class="col-md-2">
                                                                        <input type="text" id="cor_section1" name="cor_section1" class="demo" value="<?php echo $fields['secao1']; ?>" />
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="col-md-3 control-label">Cor Section 2</label>
                                                                    <div class="col-md-2">
                                                                        <input type="text" id="cor_section2" name="cor_section2" class="demo" value="<?php echo $fields['secao2']; ?>" />
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="col-md-3 control-label">Cor Section 3</label>
                                                                    <div class="col-md-2">
                                                                        <input type="text" id="cor_section3" name="cor_section3" class="demo" value="<?php echo $fields['secao3']; ?>" />
                                                                    </div>
                                                                </div>
                                                                <!--
                                                                <div class="form-group">
                                                                    <label class="col-md-3 control-label">Background Imagem 1</label>
                                                                    <div class="col-md-2">
                                                                        <input type="text" id="cor_imagem1" name="cor_imagem1" class="demo" value="<?php echo $fields['imagem1']; ?>" />
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="col-md-3 control-label">Background Imagem 2</label>
                                                                    <div class="col-md-2">
                                                                        <input type="text" id="cor_imagem2" name="cor_imagem2" class="demo" value="<?php echo $fields['imagem2']; ?>" />
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="col-md-3 control-label">Background Imagem 3</label>
                                                                    <div class="col-md-2">
                                                                        <input type="text" id="cor_imagem3" name="cor_imagem3" class="demo" value="<?php echo $fields['imagem3']; ?>" />
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="col-md-3 control-label">Background Imagem 4</label>
                                                                    <div class="col-md-2">
                                                                        <input type="text" id="cor_imagem4" name="cor_imagem4" class="demo" value="<?php echo $fields['imagem4']; ?>" />
                                                                    </div>
                                                                </div>
                                                                -->
                                                                <div class="form-group">
                                                                    <label class="col-md-3 control-label">Cor do Título</label>
                                                                    <div class="col-md-2">
                                                                        <input type="text" id="cor_fonte" name="cor_fonte" class="demo" value="<?php echo $fields['cor_fonte']; ?>" />
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="col-md-3 control-label">Cor do Texto</label>
                                                                    <div class="col-md-2">
                                                                        <input type="text" id="cor_texto" name="cor_texto" class="demo" value="<?php echo $fields['cor_texto']; ?>" />
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="col-md-3 control-label">Cor Valor</label>
                                                                    <div class="col-md-2">
                                                                        <input type="text" id="cor_valor" name="cor_valor" class="demo" value="<?php echo $fields['cor_valor']; ?>" />
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="col-md-3 control-label">Cor Parcelamento</label>
                                                                    <div class="col-md-2">
                                                                        <input type="text" id="cor_parcelamento" name="cor_parcelamento" class="demo" value="<?php echo $fields['cor_parcelamento']; ?>" />
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="col-md-3 control-label">Tamanho da Fonte</label>
                                                                    <div class="col-md-4">
                                                                        <?php echo form_dropdown('tamanho_fonte', array('16px' => '16px', '18px' => '18px', '20px' => '20px', '24px' => '24px', '28px' => '28px', '32px' => '32px'), $fields['tamanho_fonte'], 'class="form-control" id="tamanho_fonte" style="font-size:16px"'); ?>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="edital" class="control-label col-md-3">Logo</label>
                                                                    <div class="col-md-4">
                                                                        <?php echo form_upload(array("autocomplete" => "off", "name" => "logo", "id" => "logo", "class" => "form-control")); ?>
                                                                    </div>
                                                                </div> 
                                                                <div class="form-group">
                                                                    <label for="edital" class="control-label col-md-3">Background</label>
                                                                    <div class="col-md-4">
                                                                        <?php echo form_upload(array("autocomplete" => "off", "name" => "background", "id" => "background", "class" => "form-control")); ?>
                                                                    </div>
                                                                </div>       

                                                            </div>
                                                            <div class="form-actions">
                                                                <div class="row">
                                                                    <div class="col-md-offset-3 col-md-9">
                                                                        <button type="submit" class="btn green">Salvar</button>
                                                                        <a href="<?php echo $url_previous; ?>" class="btn default">Voltar</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        <?php echo form_close(); ?>
                                                        <!-- END FORM-->
                                                    </div>
                                                </div>

                                            </div>
                                        </div>


                                    </div>
                                    <!-- END PAGE CONTENT INNER -->
									
									
                                </div>
                            </div>
                            <!-- END PAGE CONTENT BODY -->
                            <!-- END CONTENT BODY -->
                        </div>
                        <!-- END CONTENT -->
                        
                    </div>
                    <!-- END CONTAINER -->
                </div>
            </div>
            
        </div>
       <!-- BEGIN FOOTER -->
	   <?php $this->load->view('footer'); ?>
	   <!-- END FOOTER -->	   
    </body>

</html>