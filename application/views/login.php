<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title>Faça seu Login</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="Preview page of Metronic Admin Theme #3 for " name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('/assets/global/plugins/font-awesome/css/font-awesome.min.css') ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('/assets/global/plugins/simple-line-icons/simple-line-icons.min.css') ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('/assets/global/plugins/bootstrap/css/bootstrap.min.css') ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css') ?>" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="<?php echo base_url('/assets/global/plugins/select2/css/select2.min.css') ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('/assets/global/plugins/select2/css/select2-bootstrap.min.css') ?>" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="<?php echo base_url('/assets/global/css/components.min.css') ?>" rel="stylesheet" id="style_components" type="text/css" />
        <link href="<?php echo base_url('/assets/global/css/plugins.min.css') ?>" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN PAGE LEVEL STYLES -->
        <link href="<?php echo base_url('/assets/pages/css/login-3.min.css') ?>" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="favicon.ico" /> 
        <style type="text/css">
            .alert-danger p{ color: #e73d4a!important; }
            .alert-success p{ color:#27a4b0!important; }
        </style>
    </head>
    <!-- END HEAD -->

    <body class=" login">
        <!-- BEGIN LOGO -->
        <div class="logo">
            <a href="index.html">
                <img src="<?php echo base_url('/assets/img/logo.png'); ?>" alt="" /> </a>
        </div>
        <!-- END LOGO -->
        <!-- BEGIN LOGIN -->
        <div class="content">
            <?php $this->load->view('messages'); ?>
            <!-- BEGIN LOGIN FORM --> 
            <?php if(!empty($mensagem)): ?>
            <div class="alert alert-danger">
                <button class="close" data-close="alert"></button>
                <span> <?php echo $mensagem; ?></span>
            </div>  
            <?php endif; ?>        
            <?php echo form_open('login/autenticar', 'class="login-form" id="formlogin"'); ?>
                <h3 class="form-title">Login</h3>
                <div class="alert alert-danger display-hide">
                    <button class="close" data-close="alert"></button>
                    <span> Entre com seu usuário e senha. </span>
                </div>
                <div class="form-group">
                    <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                    <label class="control-label visible-ie8 visible-ie9">Username</label>
                    <div class="input-icon">
                        <i class="fa fa-user"></i>                        
                        <?php echo form_input(array("type" => "text", "placeholder" => "Username", "autocomplete" => "off", "name" => "username", "id" => "username", "class" => "form-control placeholder-no-fix")); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">Senha</label>
                    <div class="input-icon">
                        <i class="fa fa-lock"></i>
                        <?php echo form_input(array("type" => "password", "placeholder" => "Senha", "autocomplete" => "off", "name" => "password", "id" => "password", "class" => "form-control placeholder-no-fix")); ?>                       
                    </div>
                </div>
                <div class="form-actions">                    
                    <?php echo form_button(array("class" => "btn green pull-right", "content" => "Entrar", "type" => "submit")); ?>
                </div>
                <div class="forget-password">
                    <h4>Esqueceu sua senha ?</h4>
                    <p> Clique
                        <a href="javascript:;" id="forget-password"> Aqui </a> Para recuperar sua senha. </p>
                </div>
            <?php echo form_close(); ?>
            <!-- END LOGIN FORM -->
			
            <!-- BEGIN FORGOT PASSWORD FORM -->
            <?php echo form_open('login/recovery', 'class="forget-form" id="formRecover"'); ?>            
                <h3>Esqueceu sua senha ?</h3>
                <p> Entre com seu email abaixo para recuperar sua senha. </p>
                <div class="form-group">
                    <div class="input-icon">
                        <i class="fa fa-envelope"></i>
                        <?php echo form_input(array("type" => "text", "placeholder" => "Email", "autocomplete" => "off", "name" => "email_recovery", "id" => "email_recovery", "class" => "form-control placeholder-no-fix")); ?>                       
                    </div>
                </div>
                <div class="form-actions">
                    <?php echo form_button(array("id" => "back-btn", "class" => "btn grey-salsa btn-outline", "content" => "Voltar", "type" => "button")); ?>
                    <?php echo form_button(array("class" => "btn green pull-right", "content" => "Enviar", "type" => "submit")); ?>                    
                </div>
            <?php echo form_close(); ?>
            <!-- END FORGOT PASSWORD FORM -->
           
        </div>
        <!-- END LOGIN -->
        <!--[if lt IE 9]>
		<script src="<?php echo base_url('/assets/global/plugins/respond.min.js') ?>"></script>
		<script src="<?php echo base_url('/assets/global/plugins/excanvas.min.js') ?>"></script> 
		<script src="<?php echo base_url('/assets/global/plugins/ie8.fix.min.js') ?>"></script> 
		<![endif]-->
        <!-- BEGIN CORE PLUGINS -->
        <script src="<?php echo base_url('/assets/global/plugins/jquery.min.js') ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('/assets/global/plugins/bootstrap/js/bootstrap.min.js') ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('/assets/global/plugins/js.cookie.min.js') ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js') ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('/assets/global/plugins/jquery.blockui.min.js') ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js') ?>" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="<?php echo base_url('/assets/global/plugins/jquery-validation/js/jquery.validate.min.js') ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('/assets/global/plugins/jquery-validation/js/additional-methods.min.js') ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('/assets/global/plugins/select2/js/select2.full.min.js') ?>" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="<?php echo base_url('/assets/global/scripts/app.min.js') ?>" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="<?php echo base_url('/assets/pages/scripts/login.min.js') ?>" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <!-- END THEME LAYOUT SCRIPTS -->
        <script>
            $(document).ready(function()
            {
                $('#clickmewow').click(function()
                {
                    $('#radio1003').attr('checked', 'checked');
                });
            })
        </script>
    </body>

</html>