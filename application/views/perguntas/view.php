<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="pt-br" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="pt-br" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="pt-br">
    <!--<![endif]-->
    <?php $this->load->view('header'); ?>

    <body class="page-container-bg-solid page-header-menu-fixed">
        <div class="page-wrapper">
            <div class="page-wrapper-row">
                <div class="page-wrapper-top">
                    
					<!-- BEGIN HEAD -->
                     <?php $this->load->view('head'); ?>
					<!-- END HEAD -->
					
                </div>
            </div>
            <div class="page-wrapper-row full-height">
                <div class="page-wrapper-middle">
				
                    <!-- BEGIN CONTAINER -->
                    <div class="page-container">
                        <!-- BEGIN CONTENT -->
                        <div class="page-content-wrapper">
                            <!-- BEGIN CONTENT BODY -->
                            <!-- BEGIN PAGE HEAD-->
                            <div class="page-head">
                                <div class="container-fluid">
                                    <!-- BEGIN PAGE TITLE -->
                                    <div class="page-title">
                                        <h1><?php echo $titulo_page; ?></h1>
                                    </div>
                                    <!-- END PAGE TITLE -->
                                </div>
                            </div>
                            <!-- END PAGE HEAD-->
                            <!-- BEGIN PAGE CONTENT BODY -->
                            <div class="page-content">

                                

                                <div class="container-fluid">
                                   
								   <!-- BEGIN PAGE BREADCRUMBS -->
                                    <ul class="page-breadcrumb breadcrumb">
                                        <li>
                                            <a href="<?php echo base_url(); ?>">Home</a>
                                            <i class="fa fa-circle"></i>
                                        </li>
                                        <?php if(!empty($previous_page)) { ?>
                                        <li>
                                            <a href="<?php echo $url_previous; ?>"><?php echo $previous_page; ?></a>
                                            <i class="fa fa-circle"></i>
                                        </li>                 
                                        <?php } ?>
                                        <li>
                                            <span><?php echo $current_page; ?></span>
                                        </li>
                                    </ul>
                                    <!-- END PAGE BREADCRUMBS -->
									
									
                                    <!-- BEGIN PAGE CONTENT INNER -->
                                    <div class="page-content-inner">
                                        
                                        <div class="row">
                                            <div class="col-md-12">

                                                <div class="portlet light bordered">
                                                    <?php $this->load->view('messages'); ?>
                                                    <div class="portlet-body form">
                                                        <!-- BEGIN FORM-->
                                                        <?php echo form_open_multipart('', 'class="form-horizontal" id="formuser"'); ?>
                                                            <div class="form-body">
                                                                <?php if($fields->formtipo == 'form1'){ ?>
                                                                    <div class="form-group">
                                                                        <label class="col-md-3 control-label">Pergunta</label>
                                                                        <div class="col-md-4" style=>
                                                                            <?php echo nl2br($fields->pergunta); ?>
                                                                        </div>
                                                                    </div>
                                                                <?php }else{ ?>
                                                                    <div class="form-group">
                                                                        <label class="col-md-3 control-label">Nome Completo</label>
                                                                        <div class="col-md-4" style="padding-top:8px">
                                                                            <?php echo $fields->nome; ?>
                                                                        </div>
                                                                    </div>  
                                                                    <div class="form-group">
                                                                        <label class="col-md-3 control-label">Data de Nascimento</label>
                                                                        <div class="col-md-4" style="padding-top:8px">
                                                                            <?php echo datebr($fields->nascimento); ?>
                                                                        </div>
                                                                    </div>    
                                                                    <div class="form-group">
                                                                        <label class="col-md-3 control-label">Hora de Nascimento</label>
                                                                        <div class="col-md-4" style="padding-top:8px">
                                                                            <?php echo $fields->hora; ?>
                                                                        </div>
                                                                    </div>  
                                                                    <div class="form-group">
                                                                        <label class="col-md-3 control-label">Cidade</label>
                                                                        <div class="col-md-4" style="padding-top:8px">
                                                                            <?php echo $fields->cidade; ?>
                                                                        </div>
                                                                    </div>  
                                                                    <div class="form-group">
                                                                        <label class="col-md-3 control-label">Planeta</label>
                                                                        <div class="col-md-4" style="padding-top:8px">
                                                                            <?php echo $fields->planeta; ?>
                                                                        </div>
                                                                    </div> 
                                                                <?php } ?>                                                                
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">Resposta</label>
                                                                    <div class="col-md-8">
                                                                         <?php echo form_textarea(array("placeholder" => "Resposta", "autocomplete" => "off", "name" => "resposta", "id" => "resposta", "class" => "form-control", "value" => $fields->resposta)); ?>
                                                                    </div>
                                                                </div>                                                                
                                                                <div class="form-group">
                                                                    <label class="col-sm-3 control-label">
                                                                        Anexos
                                                                    </label>
                                                                    <div class="col-sm-9">
                                                                        <span class="btn btn-default btn-file">
                                                                            <input id="anexos" name="anexos[]" type="file" class="file" multiple data-show-upload="true" data-show-caption="true">
                                                                        </span>
                                                                    </div>
                                                                </div> 
                                                                <?php if(!empty($anexos)){ ?>
                                                                    <div class="col-md-offset-3"> 
                                                                        <h3>Anexos</h3>
                                                                        <?php foreach ($anexos as $res){ ?>
                                                                            <div style="margin-bottom:10px"><a href="<?php echo str_replace('/painel', '', base_url("/assets/anexos/".$res->arquivo)); ?>" title="Ver imagem" target="_blank" style="color:#333333"><i class="fa fa-file-o"></i> <?php echo $res->arquivo; ?> </a><br />
                                                                            <a href="<?php echo base_url('perguntas/anexo/'.$res->id.'/'.$fields->id); ?>" onclick="return confirm('Deseja realmente excluir?');" style="color:red;">
                                                                                        <i class="icon-trash"></i> Deletar </a>
                                                                            </div>
                                                                        <?php } ?>
                                                                    </div>
                                                                <?php } ?>
                                                            </div>
                                                            <div class="form-actions">
                                                                <div class="row">
                                                                    <div class="col-md-offset-3 col-md-9">
                                                                        <button type="submit" class="btn green">Salvar</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        <?php echo form_close(); ?>
                                                        <!-- END FORM-->
                                                    </div>
                                                </div>

                                            </div>
                                        </div>


                                    </div>
                                    <!-- END PAGE CONTENT INNER -->
									
									
                                </div>
                            </div>
                            <!-- END PAGE CONTENT BODY -->
                            <!-- END CONTENT BODY -->
                        </div>
                        <!-- END CONTENT -->
                        
                    </div>
                    <!-- END CONTAINER -->
                </div>
            </div>
            
        </div>
       <!-- BEGIN FOOTER -->
        <?php
            $ext['extra_js'] = '
            <!-- BEGIN LEVEL PLUGINS -->
            <script src="'.base_url().'assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
            <script src="'.base_url().'assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
            <script src="'.base_url().'assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
            <script src="'.base_url().'assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
            <script src="'.base_url().'assets/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js" type="text/javascript"></script>
            <script src="'.base_url().'assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js" type="text/javascript"></script>
            <script src="'.base_url().'assets/global/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>
            <script src="'.base_url().'assets/global/plugins/bootstrap-markdown/lib/markdown.js" type="text/javascript"></script>
            <script src="'.base_url().'assets/global/plugins/bootstrap-markdown/js/bootstrap-markdown.js" type="text/javascript"></script>
            <script src="'.base_url().'assets/pages/scripts/form-validation.js" type="text/javascript"></script>
            <!-- BEGIN PAGE LEVEL PLUGINS -->
            <script src="'.base_url().'assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js" type="text/javascript"></script>
            <script src="'.base_url().'assets/global/plugins/jquery.input-ip-address-control-1.0.min.js" type="text/javascript"></script>
            <!-- END PAGE LEVEL PLUGINS -->
            <!-- BEGIN PAGE LEVEL PLUGINS -->
            <script src="'.base_url().'assets/pages/scripts/form-input-mask.js" type="text/javascript"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.min.js" type="text/javascript"></script>
            <!-- END PAGE LEVEL PLUGINS -->';
            $this->load->view('footer', $ext); 
        ?>
        <script type="text/javascript">
            $('#texto, #texto_curto').wysihtml5({
              "stylesheets": ["lib/css/wysiwyg-color.css", "lib/css/highlight/github.css"], // CSS stylesheets to load
              "color": true, // enable text color selection
              "size": 'small', // buttons size
              "html": true, // enable button to edit HTML
              "format-code" : true // enable syntax highlighting
            });
            function convertToSlug(Text){
                return Text
                    .toLowerCase()
                    .replace(/[^\w ]+/g,'')
                    .replace(/ +/g,'-')
                    .replace(/[áàâã]/g,'a').replace(/[éèê]/g,'e').replace(/[óòôõ]/g,'o').replace(/[úùû]/g,'u');
            }
            $('#nome').keyup(function(){
               $('#slug').val(convertToSlug($('#nome').val()));
            });
            $('#valor').mask('#00.00', {reverse: true});
        </script>
       <!-- END FOOTER -->     
    </body>

</html>