<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="pt-br">
    <!--<![endif]-->
    <?php
        $ext['extra_css'] = '<!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="'.base_url().'assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
        <link href="'.base_url().'assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
        <link href="'.base_url().'assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->'; 
        $this->load->view('header', $ext); 
    ?>

    <body class="page-container-bg-solid page-header-menu-fixed">
        <div class="page-wrapper">
            <div class="page-wrapper-row">
                <div class="page-wrapper-top">
                    
					<!-- BEGIN HEAD -->
                     <?php $this->load->view('head'); ?>
					<!-- END HEAD -->
					
                </div>
            </div>
            <div class="page-wrapper-row full-height">
                <div class="page-wrapper-middle">
				
                    <!-- BEGIN CONTAINER -->
                    <div class="page-container">
                        <!-- BEGIN CONTENT -->
                        <div class="page-content-wrapper">
                            <!-- BEGIN CONTENT BODY -->
                            <!-- BEGIN PAGE HEAD-->
                            <div class="page-head">
                                <div class="container-fluid">
                                    <!-- BEGIN PAGE TITLE -->
                                    <div class="page-title">
                                        <h1><?php echo $titulo_page; ?></h1>
                                    </div>
                                    <!-- END PAGE TITLE -->
                                </div>
                            </div>
                            <!-- END PAGE HEAD-->
                            <!-- BEGIN PAGE CONTENT BODY -->
                            <div class="page-content">
                                <div class="container-fluid">
                                   
								   <!-- BEGIN PAGE BREADCRUMBS -->
                                    <ul class="page-breadcrumb breadcrumb">
                                        <li>
                                            <a href="<?php echo base_url(); ?>">Home</a>
                                            <i class="fa fa-circle"></i>
                                        </li>
                                        <li>
                                            <span><?php echo $current_page; ?></span>
                                        </li>
                                    </ul>
                                    <!-- END PAGE BREADCRUMBS -->
									
									
                                    <!-- BEGIN PAGE CONTENT INNER -->
                                    <div class="page-content-inner">
                                        <div class="row">
                                            <div class="col-md-12">
                                            
                                            
                                                
                                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                                <div class="portlet light ">
                                                    <?php $this->load->view('messages'); ?>
                                                    <div class="portlet-title">
                                                        <div class="actions">
                                                            
                                                                <a href="<?php echo base_url($url_add); ?>" class="btn btn green"><i class="fa fa-plus"></i> Adicionar</a>
                                                          
                                                            <div class="btn-group">
                                                                <a class="btn blue" href="javascript:;" data-toggle="dropdown" aria-expanded="false">
                                                                    <i class="fa fa-tool"></i>
                                                                    <span class="hidden-xs"> Ferramentas </span>
                                                                    <i class="fa fa-angle-down"></i>
                                                                </a>
                                                                <ul class="dropdown-menu pull-right" id="sample_1_tools">
                                                                    <li>
                                                                        <a href="javascript:;" data-action="0" class="tool-action">
                                                                            <i class="icon-printer"></i> Imprimir</a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="javascript:;" data-action="1" class="tool-action">
                                                                            <i class="icon-doc"></i> Exportar PDF</a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="javascript:;" data-action="2" class="tool-action">
                                                                            <i class="icon-paper-clip"></i> Exportar Excel</a>
                                                                    </li>
    
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="portlet-body">

                                                        <table class="table table-striped table-bordered table-hover table-checkable order-column" id="cadastrostable">
                                                            <thead>
                                                                <tr>
                                                                    <th> # </th>
                                                                    <th> Data </th>
                                                                    <th> Nome </th>
                                                                    <th> Email </th>
                                                                    <th> Empresa </th>
                                                                    <th> Site </th>
                                                                    <th> Cargo </th>
                                                                    <th> Departamento </th>
                                                                    <th> Telefone </th>
                                                                    <th> Celular </th>
                                                                    <th> Skype </th>
                                                                    <th> Endereço </th>
                                                                    <th> Ações </th>
                                                                </tr>
                                                            </thead>
                                                            
                                                            <tbody>
                                                            <?php foreach ($empresa as $res){ ?>
																<?php #if($res->id == 1 OR $res->id == 6) continue; ?>
                                                                <tr class="gradeX">
                                                                    <td><?php echo $res->id; ?></td>
                                                                     <td><?php echo datebr($res->data); ?></td>
                                                                    <td><?php echo $res->nome; ?></td>
                                                                    <td>
                                                                        <a href="mailto:<?php echo $res->email; ?>"> <?php echo $res->email; ?> </a>
                                                                    </td>
                                                                    <td><?php echo $res->empresa; ?></td>
                                                                    <td><a href="http://<?php echo $res->site; ?>" title="<?php echo $res->nome; ?>" target="_blank"><?php echo $res->site; ?></a></td>
                                                                    <td><?php echo $res->cargo; ?></td>
                                                                    <td><?php echo $res->departamento; ?></td>
                                                                    <td><?php echo $res->telefone; ?></td>
                                                                    <td><?php echo $res->celular; ?></td>
                                                                    <td><?php echo $res->skype; ?></td>
                                                                    <td><?php echo $res->endereco; ?></td>
                                                                    <td>
                                                                        <div class="btn-group">
                                                                            <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Ações
                                                                                <i class="fa fa-angle-down"></i>
                                                                            </button>
                                                                            <ul class="dropdown-menu pull-left" role="menu">
                                                                                <li>
                                                                                    <a href="<?php echo base_url($url_delete.$res->id); ?>" onclick="return confirm('Deseja realmente excluir?');">
                                                                                        <i class="icon-trash"></i> Deletar </a>
                                                                                </li>
    
                                                                            </ul>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <?php } ?>
                                                                
                                                                
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <!-- END EXAMPLE TABLE PORTLET-->


                                            </div>
                                        </div>
                                    </div>
                                    <!-- END PAGE CONTENT INNER -->
									
									
                                </div>
                            </div>
                            <!-- END PAGE CONTENT BODY -->
                            <!-- END CONTENT BODY -->
                        </div>
                        <!-- END CONTENT -->
                       
                    </div>
                    <!-- END CONTAINER -->
                </div>
            </div>
            
        </div>
       <!-- BEGIN FOOTER -->
	   <?php
        $ext['extra_js'] = '
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="'.base_url().'assets/global/scripts/datatable.js" type="text/javascript"></script>
        <script src="'.base_url().'assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
        <script src="'.base_url().'assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
        <script src="'.base_url().'assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS --> 
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
       
        <script src="'.base_url().'assets/pages/scripts/table-datatables-buttons.js" type="text/javascript"></script>        
        <!-- END PAGE LEVEL SCRIPTS -->'; 
            $this->load->view('footer', $ext); 
        ?>
	   <!-- END FOOTER -->	   
	   
    </body>

</html>