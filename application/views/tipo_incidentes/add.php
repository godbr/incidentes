<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="pt-br" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="pt-br" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="pt-br">
    <!--<![endif]-->
    <?php $this->load->view('header'); ?>

    <body class="page-container-bg-solid page-header-menu-fixed">
        <div class="page-wrapper">
            <div class="page-wrapper-row">
                <div class="page-wrapper-top">
                    
					<!-- BEGIN HEAD -->
                     <?php $this->load->view('head'); ?>
					<!-- END HEAD -->
					
                </div>
            </div>
            <div class="page-wrapper-row full-height">
                <div class="page-wrapper-middle">
				
                    <!-- BEGIN CONTAINER -->
                    <div class="page-container">
                        <!-- BEGIN CONTENT -->
                        <div class="page-content-wrapper">
                            <!-- BEGIN CONTENT BODY -->
                            <!-- BEGIN PAGE HEAD-->
                            <div class="page-head">
                                <div class="container-fluid">
                                    <!-- BEGIN PAGE TITLE -->
                                    <div class="page-title">
                                        <h1><?php echo $titulo_page; ?></h1>
                                    </div>
                                    <!-- END PAGE TITLE -->
                                </div>
                            </div>
                            <!-- END PAGE HEAD-->
                            <!-- BEGIN PAGE CONTENT BODY -->
                            <div class="page-content">

                                

                                <div class="container-fluid">
                                   
								   <!-- BEGIN PAGE BREADCRUMBS -->
                                    <ul class="page-breadcrumb breadcrumb">
                                        <li>
                                            <a href="<?php echo base_url(); ?>">Home</a>
                                            <i class="fa fa-circle"></i>
                                        </li>
                                        <?php if(!empty($previous_page)) { ?>
                                        <li>
                                            <a href="<?php echo $url_previous; ?>"><?php echo $previous_page; ?></a>
                                            <i class="fa fa-circle"></i>
                                        </li>                 
                                        <?php } ?>
                                        <li>
                                            <span><?php echo $current_page; ?></span>
                                        </li>
                                    </ul>
                                    <!-- END PAGE BREADCRUMBS -->
									
									
                                    <!-- BEGIN PAGE CONTENT INNER -->
                                    <div class="page-content-inner">
                                        
                                        <div class="row">
                                            <div class="col-md-12">

                                                <div class="portlet light bordered">
                                                    <?php $this->load->view('messages'); ?>
                                                    <div class="portlet-body form">
                                                        <!-- BEGIN FORM-->
                                                        <?php echo form_open_multipart('', 'class="form-horizontal" id="incidentesForm"'); ?>
                                                            <div class="form-body">    
                                                                <div class="form-group">
                                                                    <label class="col-md-3 control-label">Nome</label>
                                                                    <div class="col-md-4">
                                                                        <?php 
                                                                        if(!empty($id)){
                                                                            echo form_input(array("type" => "text", "placeholder" => "Nome", "autocomplete" => "off", "name" => "nome", "id" => "nome", "class" => "form-control", "required" => "required","value" => $fields['nome'])); 
                                                                        }else{
                                                                            echo form_input(array("type" => "text", "placeholder" => "Nome", "autocomplete" => "off", "name" => "nome", "id" => "nome", "class" => "form-control", "required" => "required")); 
                                                                        }
                                                                        ?>                                                                       
                                                                    </div>
                                                                </div>                                                        
                                                            </div>

                                                            <div class="form-actions">
                                                                <div class="row">
                                                                    <div class="col-md-offset-3 col-md-9">
                                                                        <button type="submit" class="btn green">Salvar</button>
                                                                        <a href="<?php echo $url_previous; ?>" class="btn default">Voltar</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        <?php echo form_close(); ?>
                                                        <!-- END FORM-->
                                                    </div>
                                                </div>

                                            </div>
                                        </div>


                                    </div>
                                    <!-- END PAGE CONTENT INNER -->
									
									
                                </div>
                            </div>
                            <!-- END PAGE CONTENT BODY -->
                            <!-- END CONTENT BODY -->
                        </div>
                        <!-- END CONTENT -->
                        
                    </div>
                    <!-- END CONTAINER -->
                </div>
            </div>
            
        </div>
       <!-- BEGIN FOOTER -->
        <?php
            $ext['extra_js'] = '
            <!-- BEGIN LEVEL PLUGINS -->
            <script src="'.base_url().'assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
            <script src="'.base_url().'assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
            <script src="'.base_url().'assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
            <script src="'.base_url().'assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
            <script src="'.base_url().'assets/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js" type="text/javascript"></script>
            <script src="'.base_url().'assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js" type="text/javascript"></script>
            <script src="'.base_url().'assets/global/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>
            <script src="'.base_url().'assets/global/plugins/bootstrap-markdown/lib/markdown.js" type="text/javascript"></script>
            <script src="'.base_url().'assets/global/plugins/bootstrap-markdown/js/bootstrap-markdown.js" type="text/javascript"></script>
            <script src="'.base_url().'assets/pages/scripts/form-validation.js" type="text/javascript"></script>
            <!-- BEGIN PAGE LEVEL PLUGINS -->
            <script src="'.base_url().'assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js" type="text/javascript"></script>
            <script src="'.base_url().'assets/global/plugins/jquery.input-ip-address-control-1.0.min.js" type="text/javascript"></script>
            <!-- END PAGE LEVEL PLUGINS -->
            <!-- BEGIN PAGE LEVEL PLUGINS -->
            <script src="'.base_url().'assets/pages/scripts/form-input-mask.js" type="text/javascript"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.min.js" type="text/javascript"></script>
            <!-- END PAGE LEVEL PLUGINS -->';
            $this->load->view('footer', $ext); 
        ?>
	   <!-- END FOOTER -->

        <script type="text/javascript">
            $('#incidentesForm').on('submit', function(){
                if($('#nome').val() == ''){
                    alert('Campo Nome Obrigatório!');
                    event.preventDefault();
                }
            });
        </script>	   
    </body>

</html>