<div class="page-wrapper-row">
	<div class="page-wrapper-bottom">
		<!-- BEGIN FOOTER -->
		<!-- BEGIN INNER FOOTER -->
		<div class="page-footer">
			<div class="container-fluid"> <?php echo date('Y'); ?> &copy; 
				<a target="_blank" href="http://agenciatribo.com">Tribo Web</a> &nbsp;|&nbsp; Todos os direitos reservados
			</div>
		</div>
		<div class="scroll-to-top">
			<i class="icon-arrow-up"></i>
		</div>
		<!-- END INNER FOOTER -->
		<!-- END FOOTER -->
	</div>
</div>

<!--[if lt IE 9]>
<script src="<?php echo base_url('/assets/global/plugins/respond.min.js'); ?>"></script>
<script src="<?php echo base_url('/assets/global/plugins/excanvas.min.js'); ?>"></script> 
<script src="<?php echo base_url('/assets/global/plugins/ie8.fix.min.js'); ?>"></script> 
<![endif]-->
<!-- BEGIN CORE PLUGINS -->
<script src="<?php echo base_url('/assets/global/plugins/jquery.min.js'); ?>" type="text/javascript"></script>
<script src="<?php echo base_url('/assets/global/plugins/bootstrap/js/bootstrap.min.js'); ?>" type="text/javascript"></script>
<script src="<?php echo base_url('/assets/global/plugins/js.cookie.min.js" type="text/javascript'); ?>"></script>
<script src="<?php echo base_url('/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js'); ?>" type="text/javascript"></script>
<script src="<?php echo base_url('/assets/global/plugins/jquery.blockui.min.js'); ?>" type="text/javascript"></script>
<script src="<?php echo base_url('/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js'); ?>" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
<script src="<?php echo base_url('assets/pages/scripts/components-color-pickers.js'); ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/global/plugins/jquery-minicolors/jquery.minicolors.js'); ?>" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="<?php echo base_url('/assets/global/scripts/app.min.js'); ?>" type="text/javascript"></script>
<!-- END THEME GLOBAL SCRIPTS -->
<?php if(!empty($extra_js)){ echo $extra_js; } ?>
<!-- BEGIN THEME LAYOUT SCRIPTS -->
<script src="<?php echo base_url('/assets/layouts/layout3/scripts/layout.min.js'); ?>" type="text/javascript"></script>
<script src="<?php echo base_url('/assets/layouts/layout3/scripts/demo.min.js'); ?>" type="text/javascript"></script>
<!-- END THEME LAYOUT SCRIPTS -->
<script>
    $(document).ready( function() {

      $('.colordemo').each( function() {
        //
        // Dear reader, it's actually very easy to initialize MiniColors. For example:
        //
        //  $(selector).minicolors();
        //
        // The way I've done it below is just for the demo, so don't get confused
        // by it. Also, data- attributes aren't supported at this time...they're
        // only used for this demo.
        //
        $(this).minicolors({
          control: $(this).attr('data-control') || 'hue',
          defaultValue: $(this).attr('data-defaultValue') || '',
          format: $(this).attr('data-format') || 'hex',
          keywords: $(this).attr('data-keywords') || '',
          inline: $(this).attr('data-inline') === 'true',
          letterCase: $(this).attr('data-letterCase') || 'lowercase',
          opacity: $(this).attr('data-opacity'),
          position: $(this).attr('data-position') || 'bottom',
          swatches: $(this).attr('data-swatches') ? $(this).attr('data-swatches').split('|') : [],
          change: function(value, opacity) {
            if( !value ) return;
            if( opacity ) value += ', ' + opacity;
            if( typeof console === 'object' ) {
              console.log(value);
            }
          },
          theme: 'bootstrap'
        });

      });

    });
  </script>
