<?php if ($this->session->flashdata('success') == TRUE): ?>
<div class="alert alert-success">
	<div class="container" style="margin:0">
		<?= $this->session->flashdata('success'); ?>	
	</div>
</div>
<?php endif; ?>
<?php if ($this->session->flashdata('error') == TRUE): ?>
<div class="alert alert-danger">
	<div class="container" style="margin:0">
		<?= $this->session->flashdata('error'); ?>
	</div>
</div>
<?php endif; ?>