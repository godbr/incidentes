<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="pt-br" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="pt-br" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="pt-br">
    <!--<![endif]-->
    <?php $this->load->view('header'); ?>

    <body class="page-container-bg-solid page-header-menu-fixed">
        <div class="page-wrapper">
            <div class="page-wrapper-row">
                <div class="page-wrapper-top">
                    
					<!-- BEGIN HEAD -->
                     <?php $this->load->view('head'); ?>
					<!-- END HEAD -->
					
                </div>
            </div>
            <div class="page-wrapper-row full-height">
                <div class="page-wrapper-middle">
				
                    <!-- BEGIN CONTAINER -->
                    <div class="page-container">
                        <!-- BEGIN CONTENT -->
                        <div class="page-content-wrapper">
                            <!-- BEGIN CONTENT BODY -->
                            <!-- BEGIN PAGE HEAD-->
                            <div class="page-head">
                                <div class="container-fluid">
                                    <!-- BEGIN PAGE TITLE -->
                                    <div class="page-title">
                                        <h1><?php echo $titulo_page; ?></h1>
                                    </div>
                                    <!-- END PAGE TITLE -->
                                </div>
                            </div>
                            <!-- END PAGE HEAD-->
                            <!-- BEGIN PAGE CONTENT BODY -->
                            <div class="page-content">

                                

                                <div class="container-fluid">
                                   
								   <!-- BEGIN PAGE BREADCRUMBS -->
                                    <ul class="page-breadcrumb breadcrumb">
                                        <li>
                                            <a href="<?php echo base_url(); ?>">Home</a>
                                            <i class="fa fa-circle"></i>
                                        </li>
                                        <?php if(!empty($previous_page)) { ?>
                                        <li>
                                            <a href="<?php echo $url_previous; ?>"><?php echo $previous_page; ?></a>
                                            <i class="fa fa-circle"></i>
                                        </li>                 
                                        <?php } ?>
                                        <li>
                                            <span><?php echo $current_page; ?></span>
                                        </li>
                                    </ul>
                                    <!-- END PAGE BREADCRUMBS -->
									
									
                                    <!-- BEGIN PAGE CONTENT INNER -->
                                    <div class="page-content-inner">
                                        
                                        <div class="row">
                                            <div class="col-md-12">

                                                <div class="portlet light bordered">
                                                    <?php $this->load->view('messages'); ?>
                                                    <div class="portlet-body form">
                                                        <!-- BEGIN FORM-->
                                                        <?php echo form_open_multipart('', 'class="form-horizontal" id="formuser"'); ?>
                                                            <div class="form-body">
                                                                <div class="form-group">
                                                                    <label class="col-md-3 control-label">Ordem</label>
                                                                    <div class="col-md-1">
                                                                        <?php echo form_input(array("type" => "text", "placeholder" => "Ordem", "autocomplete" => "off", "name" => "ordem", "id" => "ordem", "class" => "form-control", "value" => 0), set_value('ordem')); ?>
                                                                    </div>
                                                                </div> 
                                                                <div class="form-group">
                                                                    <label class="col-md-3 control-label">Tipo de Form</label>
                                                                    <div class="col-md-4">
                                                                    <?php echo form_dropdown('form', $option_form, $fields['form'], 'class="form-control"'); ?>
                                                                    </div>
                                                                </div>                       
                                                                <div class="form-group">
                                                                    <label class="col-md-3 control-label">Template</label>
                                                                    <div class="col-md-4">
                                                                    <?php echo form_dropdown('template', $options, $fields['template_id'], 'class="form-control"'); ?>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="col-md-3 control-label">Nome</label>
                                                                    <div class="col-md-4">
                                                                        <?php echo form_input(array("type" => "text", "placeholder" => "Nome", "autocomplete" => "off", "name" => "nome", "id" => "nome", "class" => "form-control", "value" => $fields['nome'])); ?>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="col-md-3 control-label">Slug</label>
                                                                    <div class="col-md-4">
                                                                        <?php echo form_input(array("type" => "text", "placeholder" => "Slug", "autocomplete" => "off", "name" => "slug", "id" => "slug", "class" => "form-control", "value" => $fields['slug'])); ?>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="col-md-3 control-label">Valor</label>
                                                                    <div class="col-md-4">
                                                                        <?php echo form_input(array("type" => "text", "placeholder" => "Ex. 19.90", "autocomplete" => "off", "name" => "valor", "id" => "valor", "class" => "form-control", "value" => $fields['valor'])); ?>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="col-md-3 control-label">Vídeo</label>
                                                                    <div class="col-md-4">
                                                                        <?php echo form_input(array("type" => "text", "placeholder" => "https://www.youtube.com/watch?v=2o_F6dkxyKI", "autocomplete" => "off", "name" => "video", "id" => "video", "class" => "form-control", "value" => $fields['video'])); ?>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="col-md-3 control-label">Título</label>
                                                                    <div class="col-md-4">
                                                                        <?php echo form_input(array("type" => "text", "placeholder" => "Nome", "autocomplete" => "off", "name" => "titulo", "id" => "titulo", "class" => "form-control", "value" => $fields['titulo'])); ?>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">Texto Curto</label>
                                                                    <div class="col-md-8">
                                                                         <?php echo form_textarea(array("placeholder" => "Resumo", "autocomplete" => "off", "name" => "texto_curto", "id" => "texto_curto", "class" => "form-control", "value" => $fields['texto_curto'])); ?>
                                                                    </div>
                                                                </div>  
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">Texto</label>
                                                                    <div class="col-md-8">
                                                                         <?php echo form_textarea(array("placeholder" => "Texto Completo", "autocomplete" => "off", "name" => "texto", "id" => "texto", "class" => "form-control", "value" => $fields['texto'])); ?>
                                                                    </div>
                                                                </div>  
                                                                <div class="form-group">
                                                                    <label for="featured" class="control-label col-md-3">Imagem Destaque</label>
                                                                    <div class="col-md-4">
                                                                        <?php echo form_upload(array("autocomplete" => "off", "name" => "featured", "id" => "featured", "class" => "form-control")); ?>
                                                                        <?php if(!empty($fields['featured'])){ ?>
                                                                        <div><a href="<?php echo str_replace('/painel', '', base_url("/assets/produtos/".$fields['featured'])); ?>" title="Ver imagem" target="_blank">Ver Imagem</a></div>
                                                                        <?php } ?>
                                                                    </div>
                                                                </div>  
                                                                <div class="form-group">
                                                                    <label class="col-sm-3 control-label">
                                                                        Imagens
                                                                    </label>
                                                                    <div class="col-sm-9">
                                                                        <span class="btn btn-default btn-file">
                                                                            <input id="imagens" name="imagens[]" type="file" class="file" multiple data-show-upload="true" data-show-caption="true">
                                                                        </span>
                                                                    </div>
                                                                </div>   
                                                                <?php if(!empty($img)){ ?>
                                                                    <h3 class="col-md-offset-3" style="margin-top:25px; margin-bottom:20px;">IMAGENS ADICIONADAS</h3>
                                                                    <?php $i=0; foreach($img as $rest){ ?>
                                                                        <div class="form-group">
                                                                            <label class="col-md-offset-3 control-label col-md-2">
                                                                                <img src="<?php echo str_replace('/painel', '', base_url("/assets/imagens/".$rest->imagem)); ?>" class="img-responsive" /><div style="text-align:center; padding-top:15px;">
                                                                                <a href="<?php echo base_url('produtos/imagens/'.$rest->id.'/'.$fields['id']); ?>" onclick="return confirm('Deseja realmente excluir?');" style="color:red;">
                                                                                        <i class="icon-trash"></i> Deletar Imagem </a></div>
                                                                            </label>
                                                                            <div class="col-md-2">
                                                                                <div class="form-group">
                                                                                    <label class="col-md-4 control-label">Ordem</label>
                                                                                    <div class="col-md-4">
                                                                                        <?php echo form_input(array("type" => "text", "placeholder" => "Ordem", "autocomplete" => "off", "name" => "imgs[ordem][]", "id" => "ordem_".$i, "class" => "form-control", "value" => $rest->ordem), set_value('ordem')); ?>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="form-group">
                                                                                    <label class="control-label col-md-4">Background</label>
                                                                                    <div class="col-md-4">
                                                                                        <input type="text" id="cor_<?php echo $i; ?>" name="imgs[background][]" class="demo" value="<?php echo $rest->background; ?>" />
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-3">
                                                                                 <?php echo form_textarea(array("placeholder" => "Texto Imagem", "autocomplete" => "off", "name" => "imgs[texto_imagem][]", "id" => "texto_".$i, "class" => "form-control textos", "value" => $rest->texto)); ?>
                                                                            </div>
                                                                            <input type="hidden" value="<?php echo $rest->id; ?>" name="imgs[imagem_id][]" id="imagem_id<?php echo $rest->id; ?>" />
                                                                        </div>       
                                                                    <?php $i++; } ?> 
                                                                    <hr />  
                                                                <?php } ?>                                                              
                                                            </div>
                                                            <div class="form-actions">
                                                                <div class="row">
                                                                    <div class="col-md-offset-3 col-md-9">
                                                                        <button type="submit" class="btn green">Salvar</button>
                                                                        <a href="<?php echo $url_previous; ?>" class="btn default">Voltar</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        <?php echo form_close(); ?>
                                                        <!-- END FORM-->
                                                    </div>
                                                </div>

                                            </div>
                                        </div>


                                    </div>
                                    <!-- END PAGE CONTENT INNER -->
									
									
                                </div>
                            </div>
                            <!-- END PAGE CONTENT BODY -->
                            <!-- END CONTENT BODY -->
                        </div>
                        <!-- END CONTENT -->
                        
                    </div>
                    <!-- END CONTAINER -->
                </div>
            </div>
            
        </div>
       <!-- BEGIN FOOTER -->
        <?php
            $ext['extra_js'] = '
            <!-- BEGIN LEVEL PLUGINS -->
            <script src="'.base_url().'assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
            <script src="'.base_url().'assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
            <script src="'.base_url().'assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
            <script src="'.base_url().'assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
            <script src="'.base_url().'assets/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js" type="text/javascript"></script>
            <script src="'.base_url().'assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js" type="text/javascript"></script>
            <script src="'.base_url().'assets/global/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>
            <script src="'.base_url().'assets/global/plugins/bootstrap-markdown/lib/markdown.js" type="text/javascript"></script>
            <script src="'.base_url().'assets/global/plugins/bootstrap-markdown/js/bootstrap-markdown.js" type="text/javascript"></script>
            <script src="'.base_url().'assets/pages/scripts/form-validation.js" type="text/javascript"></script>
            <!-- BEGIN PAGE LEVEL PLUGINS -->
            <script src="'.base_url().'assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js" type="text/javascript"></script>
            <script src="'.base_url().'assets/global/plugins/jquery.input-ip-address-control-1.0.min.js" type="text/javascript"></script>
            <!-- END PAGE LEVEL PLUGINS -->
            <!-- BEGIN PAGE LEVEL PLUGINS -->
            <script src="'.base_url().'assets/pages/scripts/form-input-mask.js" type="text/javascript"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.min.js" type="text/javascript"></script>
            <!-- END PAGE LEVEL PLUGINS -->';
            $this->load->view('footer', $ext); 
        ?>
        <script type="text/javascript">
            $(document).ready(function() {
                $('#texto_curto').richText({

                    // text formatting
                    bold: true,
                    italic: true,
                    underline: true,

                    // text alignment
                    leftAlign: true,
                    centerAlign: true,
                    rightAlign: true,
                    justify: true,

                    // lists
                    ol: true,
                    ul: true,

                    // title
                    heading: true,

                    // fonts
                    fonts: true,
                    fontList: [
                    "Arial", 
                    "Arial Black", 
                    "Comic Sans MS", 
                    "Courier New", 
                    "Geneva", 
                    "Georgia", 
                    "Helvetica", 
                    "Impact", 
                    "Lucida Console", 
                    "Tahoma", 
                    "Times New Roman",
                    "Verdana"
                    ],
                    fontColor: true,
                    fontSize: true,
                    // link
                    urls: true,

                    // tables
                    table: true,

                    // code
                    removeStyles: true,
                    code: true,

                    // colors
                    colors: [],

                    // dropdowns
                    fileHTML: '',
                    imageHTML: '',

                    // translations
                    translations: {
                    'title': 'Title',
                    'white': 'White',
                    'black': 'Black',
                    'brown': 'Brown',
                    'beige': 'Beige',
                    'darkBlue': 'Dark Blue',
                    'blue': 'Blue',
                    'lightBlue': 'Light Blue',
                    'darkRed': 'Dark Red',
                    'red': 'Red',
                    'darkGreen': 'Dark Green',
                    'green': 'Green',
                    'purple': 'Purple',
                    'darkTurquois': 'Dark Turquois',
                    'turquois': 'Turquois',
                    'darkOrange': 'Dark Orange',
                    'orange': 'Orange',
                    'yellow': 'Yellow',
                    'imageURL': 'Image URL',
                    'fileURL': 'File URL',
                    'linkText': 'Link text',
                    'url': 'URL',
                    'size': 'Size',
                    'responsive': 'Responsive',
                    'text': 'Text',
                    'openIn': 'Open in',
                    'sameTab': 'Same tab',
                    'newTab': 'New tab',
                    'align': 'Align',
                    'left': 'Left',
                    'center': 'Center',
                    'right': 'Right',
                    'rows': 'Rows',
                    'columns': 'Columns',
                    'add': 'Add',
                    'pleaseEnterURL': 'Please enter an URL',
                    'videoURLnotSupported': 'Video URL not supported',
                    'pleaseSelectImage': 'Please select an image',
                    'pleaseSelectFile': 'Please select a file',
                    'bold': 'Bold',
                    'italic': 'Italic',
                    'underline': 'Underline',
                    'alignLeft': 'Align left',
                    'alignCenter': 'Align centered',
                    'alignRight': 'Align right',
                    'addOrderedList': 'Add ordered list',
                    'addUnorderedList': 'Add unordered list',
                    'addHeading': 'Add Heading/title',
                    'addFont': 'Add font',
                    'addFontColor': 'Add font color',
                    'addFontSize' : 'Add font size',
                    'addImage': 'Add image',
                    'addVideo': 'Add video',
                    'addFile': 'Add file',
                    'addURL': 'Add URL',
                    'addTable': 'Add table',
                    'removeStyles': 'Remove styles',
                    'code': 'Show HTML code',
                    'undo': 'Undo',
                    'redo': 'Redo',
                    'close': 'Close'
                    },

                    // privacy
                    youtubeCookies: false,

                    // developer settings
                    useSingleQuotes: false,
                    height: 0,
                    heightPercentage: 0,
                    id: "",
                    class: "",
                    useParagraph: false
                    });
                    $('#texto').richText({

                    // text formatting
                    bold: true,
                    italic: true,
                    underline: true,

                    // text alignment
                    leftAlign: true,
                    centerAlign: true,
                    rightAlign: true,
                    justify: true,

                    // lists
                    ol: true,
                    ul: true,

                    // title
                    heading: true,

                    // fonts
                    fonts: true,
                    fontList: [
                    "Arial", 
                    "Arial Black", 
                    "Comic Sans MS", 
                    "Courier New", 
                    "Geneva", 
                    "Georgia", 
                    "Helvetica", 
                    "Impact", 
                    "Lucida Console", 
                    "Tahoma", 
                    "Times New Roman",
                    "Verdana"
                    ],
                    fontColor: true,
                    fontSize: true,

                    // uploads
                    imageUpload: true,
                    fileUpload: true,

                    // media
                    videoEmbed: true,

                    // link
                    urls: true,

                    // tables
                    table: true,

                    // code
                    removeStyles: true,
                    code: true,

                    // colors
                    colors: [],

                    // dropdowns
                    fileHTML: '',
                    imageHTML: '',

                    // translations
                    translations: {
                    'title': 'Title',
                    'white': 'White',
                    'black': 'Black',
                    'brown': 'Brown',
                    'beige': 'Beige',
                    'darkBlue': 'Dark Blue',
                    'blue': 'Blue',
                    'lightBlue': 'Light Blue',
                    'darkRed': 'Dark Red',
                    'red': 'Red',
                    'darkGreen': 'Dark Green',
                    'green': 'Green',
                    'purple': 'Purple',
                    'darkTurquois': 'Dark Turquois',
                    'turquois': 'Turquois',
                    'darkOrange': 'Dark Orange',
                    'orange': 'Orange',
                    'yellow': 'Yellow',
                    'imageURL': 'Image URL',
                    'fileURL': 'File URL',
                    'linkText': 'Link text',
                    'url': 'URL',
                    'size': 'Size',
                    'responsive': 'Responsive',
                    'text': 'Text',
                    'openIn': 'Open in',
                    'sameTab': 'Same tab',
                    'newTab': 'New tab',
                    'align': 'Align',
                    'left': 'Left',
                    'center': 'Center',
                    'right': 'Right',
                    'rows': 'Rows',
                    'columns': 'Columns',
                    'add': 'Add',
                    'pleaseEnterURL': 'Please enter an URL',
                    'videoURLnotSupported': 'Video URL not supported',
                    'pleaseSelectImage': 'Please select an image',
                    'pleaseSelectFile': 'Please select a file',
                    'bold': 'Bold',
                    'italic': 'Italic',
                    'underline': 'Underline',
                    'alignLeft': 'Align left',
                    'alignCenter': 'Align centered',
                    'alignRight': 'Align right',
                    'addOrderedList': 'Add ordered list',
                    'addUnorderedList': 'Add unordered list',
                    'addHeading': 'Add Heading/title',
                    'addFont': 'Add font',
                    'addFontColor': 'Add font color',
                    'addFontSize' : 'Add font size',
                    'addImage': 'Add image',
                    'addVideo': 'Add video',
                    'addFile': 'Add file',
                    'addURL': 'Add URL',
                    'addTable': 'Add table',
                    'removeStyles': 'Remove styles',
                    'code': 'Show HTML code',
                    'undo': 'Undo',
                    'redo': 'Redo',
                    'close': 'Close'
                    },

                    // privacy
                    youtubeCookies: false,

                    // developer settings
                    useSingleQuotes: false,
                    height: 0,
                    heightPercentage: 0,
                    id: "",
                    class: "",
                    useParagraph: false
                    });
                <?php if(!empty($img)){ ?>
                    <?php $contador = 0; ?>
                    <?php foreach($img as $imgid){ ?>
                        $('#texto_<?php echo $contador; ?>').richText();
                    <?php $contador++; } ?>
                <?php } ?>
            });
            function convertToSlug(Text){
                return Text
                    .toLowerCase()
                    .replace(/[^\w ]+/g,'')
                    .replace(/ +/g,'-')
                    .replace(/[áàâã]/g,'a').replace(/[éèê]/g,'e').replace(/[óòôõ]/g,'o').replace(/[úùû]/g,'u');
            }
            $('#nome').keyup(function(){
               $('#slug').val(convertToSlug($('#nome').val()));
            });
            $('#valor').mask('#00.00', {reverse: true});
        </script>
       <!-- END FOOTER -->     
    </body>

</html>