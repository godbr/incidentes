<!-- BEGIN HEAD -->
<head>
	<meta charset="utf-8" />
	<title><?php echo $titulo_site; ?></title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta content="width=device-width, initial-scale=1" name="viewport" />
	
	<!-- META TAGS -->
	<meta name="description" content="O Guru Astral está pronto para responder diversas questões">
	<meta name="keywords" content="guru, astral, guru astral, esotérico, signo, mapa astral">
	<meta name="robots" content="index, follow">
	<meta name="revisit-after" content="1 day">
	<meta name="generator" content="N/A">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta content="Richard Itokazo" name="author" />
	<!-- END META TAGS -->

	<!-- OG TAGS -->
	<meta property="og:title" content="Guru Astral">
	<meta property="og:site_name" content="Guru Astral">
	<meta property="og:url" content="www.guruastral.net">
	<meta property="og:description" content="O Guru Astral está aqui para te ajudar a resolver o que você precisar!">
	<meta property="og:type" content="article">
	<meta property="og:image" content="http://consultoria.guruastral.net/assets/pages/img/logo2.png">
	<!-- END OG TAGS -->

	<!-- BEGIN GLOBAL MANDATORY STYLES -->
	<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url('/assets/global/plugins/font-awesome/css/font-awesome.min.css'); ?>" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url('/assets/global/plugins/simple-line-icons/simple-line-icons.min.css'); ?>" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url('/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet'); ?>" type="text/css" />
	<link href="<?php echo base_url('/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css'); ?>" rel="stylesheet" type="text/css" />
	<!-- END GLOBAL MANDATORY STYLES -->
	<!-- BEGIN THEME GLOBAL STYLES -->
	<link href="<?php echo base_url('/assets/global/css/components.min.css'); ?>" rel="stylesheet" id="style_components" type="text/css" />
	<link href="<?php echo base_url('/assets/global/css/plugins.min.css'); ?>" rel="stylesheet" type="text/css" />
	<!-- END THEME GLOBAL STYLES -->
	<!-- BEGIN THEME LAYOUT STYLES -->
	<link href="<?php echo base_url('/assets/layouts/layout3/css/layout.min.css'); ?>" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url('/assets/layouts/layout3/css/themes/default.min.css'); ?>" rel="stylesheet" type="text/css" id="style_color" />
	<link href="<?php echo base_url('/assets/layouts/layout3/css/custom.min.css'); ?>" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url('assets/global/plugins/bootstrap-colorpicker/css/colorpicker.css'); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets/global/plugins/jquery-minicolors/jquery.minicolors.css') ?>" rel="stylesheet" type="text/css" />
	<!-- END THEME LAYOUT STYLES -->
	<?php if(!empty($extra_css)){ echo $extra_css; } ?>
	<link href="<?php echo base_url('/assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css'); ?>" rel="stylesheet" type="text/css" />
	<link rel="shortcut icon" href="favicon.ico" /> 
	<style>
		.page-header .page-header-top .page-logo .logo-default {
		    margin: 7.5px 0 0;
		}
		.alert-danger p{ color: #e73d4a!important; }
		.alert-success p{ color:#27a4b0!important; }
		table.dataTable.no-footer { border:0px; }
		.none{ display:none; }
		input.demo{ padding:5px; }
	</style>
</head>
<!-- END HEAD -->