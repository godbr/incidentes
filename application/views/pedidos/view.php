<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="pt-br" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="pt-br" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="pt-br">
    <!--<![endif]-->
    <?php $this->load->view('header'); ?>

    <body class="page-container-bg-solid page-header-menu-fixed">
        <div class="page-wrapper">
            <div class="page-wrapper-row">
                <div class="page-wrapper-top">
                    
					<!-- BEGIN HEAD -->
                     <?php $this->load->view('head'); ?>
					<!-- END HEAD -->
					
                </div>
            </div>
            <div class="page-wrapper-row full-height">
                <div class="page-wrapper-middle">
				
                    <!-- BEGIN CONTAINER -->
                    <div class="page-container">
                        <!-- BEGIN CONTENT -->
                        <div class="page-content-wrapper">
                            <!-- BEGIN CONTENT BODY -->
                            <!-- BEGIN PAGE HEAD-->
                            <div class="page-head">
                                <div class="container-fluid">
                                    <!-- BEGIN PAGE TITLE -->
                                    <div class="page-title">
                                        <h1><?php echo $titulo_page; ?></h1>
                                    </div>
                                    <!-- END PAGE TITLE -->
                                </div>
                            </div>
                            <!-- END PAGE HEAD-->
                            <!-- BEGIN PAGE CONTENT BODY -->
                            <div class="page-content">

                                

                                <div class="container-fluid">
                                   
								   <!-- BEGIN PAGE BREADCRUMBS -->
                                    <ul class="page-breadcrumb breadcrumb">
                                        <li>
                                            <a href="<?php echo base_url(); ?>">Home</a>
                                            <i class="fa fa-circle"></i>
                                        </li>
                                        <?php if(!empty($previous_page)) { ?>
                                        <li>
                                            <a href="<?php echo $url_previous; ?>"><?php echo $previous_page; ?></a>
                                            <i class="fa fa-circle"></i>
                                        </li>                 
                                        <?php } ?>
                                        <li>
                                            <span><?php echo $current_page; ?></span>
                                        </li>
                                    </ul>
                                    <!-- END PAGE BREADCRUMBS -->
									
									
                                    <!-- BEGIN PAGE CONTENT INNER -->
                                    <div class="page-content-inner">
                                        
                                        <div class="row">
                                            <div class="col-md-12">

                                                <div class="portlet light bordered">
                                                    <?php $this->load->view('messages'); ?>
                                                    <div class="portlet-body form">                                                        
                                                        <?php foreach ($fields as $res){ ?>

                                                        <div class="form-body">
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label" style='text-align: right;'>Nome Completo:</label>
                                                                <div class="col-md-4" style="text-align: left;">
                                                                    <?php echo $res->nome_cliente; ?>
                                                                </div>
                                                            </div>                                                                
                                                        </div>

                                                         <div class="form-body">
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label" style='text-align: right;'>Email:</label>
                                                                <div class="col-md-4">
                                                                    <?php echo $res->email; ?>
                                                                </div>
                                                            </div>                                                                
                                                        </div>

                                                         <div class="form-body">
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label" style='text-align: right;'>Telefone:</label>
                                                                <div class="col-md-4">
                                                                    <?php echo $res->ddd." ".$res->telefone; ?>
                                                                </div>
                                                            </div>                                                                
                                                        </div>

                                                        <div class="form-body">
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label" style='text-align: right;'>Endereço:</label>
                                                                <div class="col-md-4">
                                                                    <?php echo $res->endereco; ?>, <?php echo $res->numero; ?>
                                                                </div>
                                                            </div>                                                                
                                                        </div>

                                                        <div class="form-body">
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label" style='text-align: right;'>Cep:</label>
                                                                <div class="col-md-4">
                                                                    <?php echo $res->cep; ?>
                                                                </div>
                                                            </div>                                                                
                                                        </div>

                                                        <div class="form-body">
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label" style='text-align: right;'>Produto:</label>
                                                                <div class="col-md-4">
                                                                    <?php echo $res->nome; ?>
                                                                </div>
                                                            </div>                                                                
                                                        </div>

                                                        <div class="form-body">
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label" style='text-align: right;'>Valor:</label>
                                                                <div class="col-md-4">
                                                                    <?php echo $res->valor; ?>
                                                                </div>
                                                            </div>                                                                
                                                        </div>

                                                        <div class="form-body">
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label" style='text-align: right;'>Status:</label>
                                                                <div class="col-md-4">
                                                                    <?php echo $res->status; ?>
                                                                </div>
                                                            </div>                                                                
                                                        </div>

                                                        <?php } ?>

                                                        <div class="form-actions">
                                                            <div class="row">
                                                                <div class="col-md-offset-3 col-md-9">
                                                                    <a href="<?php echo base_url('pedidos'); ?>" class="btn default">Voltar</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>


                                    </div>
                                    <!-- END PAGE CONTENT INNER -->
									
									
                                </div>
                            </div>
                            <!-- END PAGE CONTENT BODY -->
                            <!-- END CONTENT BODY -->
                        </div>
                        <!-- END CONTENT -->
                        
                    </div>
                    <!-- END CONTAINER -->
                </div>
            </div>
            
        </div>
       <!-- BEGIN FOOTER -->
        <?php
            $ext['extra_js'] = '
            <!-- BEGIN LEVEL PLUGINS -->
            <script src="'.base_url().'assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
            <script src="'.base_url().'assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
            <script src="'.base_url().'assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
            <script src="'.base_url().'assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
            <script src="'.base_url().'assets/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js" type="text/javascript"></script>
            <script src="'.base_url().'assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js" type="text/javascript"></script>
            <script src="'.base_url().'assets/global/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>
            <script src="'.base_url().'assets/global/plugins/bootstrap-markdown/lib/markdown.js" type="text/javascript"></script>
            <script src="'.base_url().'assets/global/plugins/bootstrap-markdown/js/bootstrap-markdown.js" type="text/javascript"></script>
            <script src="'.base_url().'assets/pages/scripts/form-validation.js" type="text/javascript"></script>
            <!-- BEGIN PAGE LEVEL PLUGINS -->
            <script src="'.base_url().'assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js" type="text/javascript"></script>
            <script src="'.base_url().'assets/global/plugins/jquery.input-ip-address-control-1.0.min.js" type="text/javascript"></script>
            <!-- END PAGE LEVEL PLUGINS -->
            <!-- BEGIN PAGE LEVEL PLUGINS -->
            <script src="'.base_url().'assets/pages/scripts/form-input-mask.js" type="text/javascript"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.min.js" type="text/javascript"></script>
            <!-- END PAGE LEVEL PLUGINS -->';
            $this->load->view('footer', $ext); 
        ?>
        <script type="text/javascript">
            $('#texto').wysihtml5({
              "stylesheets": ["lib/css/wysiwyg-color.css", "lib/css/highlight/github.css"], // CSS stylesheets to load
              "color": true, // enable text color selection
              "size": 'small', // buttons size
              "html": true, // enable button to edit HTML
              "format-code" : true // enable syntax highlighting
            });
            function convertToSlug(Text){
                return Text
                    .toLowerCase()
                    .replace(/[^\w ]+/g,'')
                    .replace(/ +/g,'-')
                    .replace(/[áàâã]/g,'a').replace(/[éèê]/g,'e').replace(/[óòôõ]/g,'o').replace(/[úùû]/g,'u');
            }
            $('#nome').keyup(function(){
               $('#slug').val(convertToSlug($('#nome').val()));
            });
            $('#valor').mask('#00.00', {reverse: true});
        </script>
       <!-- END FOOTER -->     
    </body>

</html>