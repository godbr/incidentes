<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="pt-br" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="pt-br" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="pt-br">
    <!--<![endif]-->
    <?php $this->load->view('header'); ?>

    <body class="page-container-bg-solid page-header-menu-fixed">
        <div class="page-wrapper">
            <div class="page-wrapper-row">
                <div class="page-wrapper-top">
                    
					<!-- BEGIN HEAD -->
                     <?php $this->load->view('head'); ?>
					<!-- END HEAD -->
					
                </div>
            </div>
            <div class="page-wrapper-row full-height">
                <div class="page-wrapper-middle">
				
                    <!-- BEGIN CONTAINER -->
                    <div class="page-container">
                        <!-- BEGIN CONTENT -->
                        <div class="page-content-wrapper">
                            <!-- BEGIN CONTENT BODY -->
                            <!-- BEGIN PAGE HEAD-->
                            <div class="page-head">
                                <div class="container-fluid">
                                    <!-- BEGIN PAGE TITLE -->
                                    <div class="page-title">
                                        <h1><?php echo $titulo_page; ?></h1>
                                    </div>
                                    <!-- END PAGE TITLE -->
                                </div>
                            </div>
                            <!-- END PAGE HEAD-->
                            <!-- BEGIN PAGE CONTENT BODY -->
                            <div class="page-content">

                                

                                <div class="container-fluid">
                                   
								   <!-- BEGIN PAGE BREADCRUMBS -->
                                    <ul class="page-breadcrumb breadcrumb">
                                        <li>
                                            <a href="<?php echo base_url(); ?>">Home</a>
                                            <i class="fa fa-circle"></i>
                                        </li>
                                        <?php if(!empty($previous_page)) { ?>
                                        <li>
                                            <a href="<?php echo $url_previous; ?>"><?php echo $previous_page; ?></a>
                                            <i class="fa fa-circle"></i>
                                        </li>                 
                                        <?php } ?>
                                        <li>
                                            <span><?php echo $current_page; ?></span>
                                        </li>
                                    </ul>
                                    <!-- END PAGE BREADCRUMBS -->
									
									
                                    <!-- BEGIN PAGE CONTENT INNER -->
                                    <div class="page-content-inner">
                                        
                                        <div class="row">
                                            <div class="col-md-12">

                                                <div class="portlet light bordered">
                                                    <?php $this->load->view('messages'); ?>
                                                    <div class="portlet-body form">
                                                        <!-- BEGIN FORM-->
                                                        <?php echo form_open('', 'class="form-horizontal" id="formuser"'); ?>
                                                            <div class="form-body">

                                                                <div class="form-group">
                                                                    <label class="col-md-3 control-label">Nome</label>
                                                                    <div class="col-md-4">
                                                                        <?php echo form_input(array("type" => "text", "placeholder" => "Nome", "autocomplete" => "off", "name" => "nome", "id" => "nome", "class" => "form-control", "value" => $fields['nome'])); ?>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="col-md-3 control-label">Email</label>
                                                                    <div class="col-md-4">
                                                                        
                                                                            <?php echo form_input(array("type" => "text", "placeholder" => "Email", "autocomplete" => "off", "name" => "email", "id" => "email", "class" => "form-control", "value" => $fields['email'])); ?>
                                                                        
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="col-md-3 control-label">Site</label>
                                                                    <div class="col-md-4">
                                                                        
                                                                            <?php echo form_input(array("type" => "text", "placeholder" => "Site", "autocomplete" => "off", "name" => "site", "id" => "site", "class" => "form-control", "value" => $fields['site'])); ?>
                                                                        
                                                                    </div>
                                                                </div>

                                                            </div>
                                                            <div class="form-actions">
                                                                <div class="row">
                                                                    <div class="col-md-offset-3 col-md-9">
                                                                        <button type="submit" class="btn green">Salvar</button>
                                                                        <a href="<?php echo $url_previous; ?>" class="btn default">Voltar</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        <?php echo form_close(); ?>
                                                        <!-- END FORM-->
                                                    </div>
                                                </div>

                                            </div>
                                        </div>


                                    </div>
                                    <!-- END PAGE CONTENT INNER -->
									
									
                                </div>
                            </div>
                            <!-- END PAGE CONTENT BODY -->
                            <!-- END CONTENT BODY -->
                        </div>
                        <!-- END CONTENT -->
                        
                    </div>
                    <!-- END CONTAINER -->
                </div>
            </div>
            
        </div>
       <!-- BEGIN FOOTER -->
	   <?php $this->load->view('footer'); ?>
	   <!-- END FOOTER -->	   
    </body>

</html>