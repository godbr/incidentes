<?php if (! defined('BASEPATH')) exit('No direct script access allowed'); 

class Tipo_incidentes_model extends MY_Model{

    function __construct() {
        parent::__construct();
        $this->table = 'tipo_incidentes';
    }

    public function get_all(){
    	$this->db->order_by('nome', 'ASC');
        $usuario = $this->db->get("tipo_incidentes");
        return $usuario->result();
    }	

}