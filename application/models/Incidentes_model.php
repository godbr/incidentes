<?php if (! defined('BASEPATH')) exit('No direct script access allowed'); 

class Incidentes_model extends MY_Model{

    function __construct() {
        parent::__construct();
        $this->table = 'incidentes';
    }

    public function get_all(){
        $this->db->order_by("id", "DESC");
        $usuario = $this->db->get('incidentes');
        return $usuario->result();
    }
    public function list_all($id=NULL){

        $Where = '';

        if(!empty($id)){
            $Where = " AND id = ".intval($id);
        }

        $str = "
            SELECT 
                i.id as id,
                t.id as tipo_id,
                t.nome as tipo,
                i.*
            FROM 
                incidentes i JOIN tipo_incidentes t ON t.id = i.tipo_id
            WHERE
                i.id IS NOT NULL $Where

        ";
        $sql = $this->db->query($str);

        $result = $sql->result();

        return $result;
    }
    function ExcluirCascade($id) {
        if(is_null($id))
            return false;
        $this->db->where('tipo_id', $id);
        return $this->db->delete($this->table);
    }

}