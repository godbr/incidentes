<?php if (! defined('BASEPATH')) exit('No direct script access allowed'); 

class Anexos_model extends MY_Model{

    function __construct() {
        parent::__construct();
        $this->table = 'anexos';
    }

    public function get_all(){
        $usuario = $this->db->get("anexos");
        return $usuario->result();
    }
    public function list_all($id=NULL){

        $Where = '';

        if(!empty($id)){
            $Where = " AND pergunta_id = ".intval($id);
        }

        $str = "
            SELECT 
                *
            FROM 
                anexos
            WHERE
                pergunta_id IS NOT NULL $Where

        ";
        $sql = $this->db->query($str);

        $result = $sql->result();

        return $result;
    }

}