var TableDatatablesButtons = function () {

    var initTable4 = function () {
        var table = $('#incidentestable');

        var oTable = table.dataTable({

            // Internationalisation. For more info refer to http://datatables.net/manual/i18n
            "language": {
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "Nenhum dado a ser exibido.",
                "info": "Mostrando _START_ até _END_ de _TOTAL_ registros",
                "infoEmpty": "Nenhum dado foi encontrado",
                "infoFiltered": "(filtered1 de _MAX_ resultados totais)",
                "lengthMenu": "_MENU_ Registros",
                "search": "Pesquisar:",
                "zeroRecords": "A busca não encontrou nada."
            },

            // Or you can use remote translation file
            //"language": {
            //   url: '//cdn.datatables.net/plug-ins/3cfcc339e89/i18n/Portuguese.json'
            //},


            buttons: [
                { extend: 'print', text: '<i class="icon-printer"></i> Imprimir', className: 'btn dark', 
				exportOptions: {
                    columns: [ 0, 1, 2, 3, 4, 5 ]
                },
				customize: function ( win ) {
                    $(win.document.body)
                        .css( 'font-size', '10pt' );
 
                    $(win.document.body).find( 'table' )
                        .addClass( 'compact' )
                        .css( 'font-size', 'inherit' );
                }
				},
                { extend: 'pdf', text:'<i class="icon-doc"></i> PDF', className: 'btn green', title: 'lista-admin-web', 
				exportOptions: {
                    columns: [ 0, 1, 2, 3, 4, 5 ]
                }},
                { extend: 'excel', text:'<i class="icon-paper-clip"></i> EXCEL', className: 'btn yellow', title: 'lista-admin-web',
				exportOptions: {
                    columns: [ 0, 1, 2, 3, 4, 5 ]
                }}
				/*,
                { extend: 'csv', className: 'btn purple', title: 'lista-admin-web',
				exportOptions: {
                    columns: [ 1, 2, 3, 4 ]
                }}*/
            ],
			
			
			
            // setup responsive extension: http://datatables.net/extensions/responsive/
            responsive: true,

            "ordering": false,
            "paging": false,
			"bInfo" : false,
            "order": [
                [0, 'asc']
            ],
            
            "lengthMenu": [
                [1, 10, 15, 20, -1],
                [5, 10, 15, 20, "All"] // change per page values here
            ],
			lengthChange: false,
            // set the initial value
            "pageLength": 20,
			"columnDefs": [
                {  // set default column settings
                    'orderable': false,
                    'targets': [0]
                }, 
                {
                    "searchable": false,
                    "targets": [0]
                },
                {
                    "className": "dt-right", 
                    //"targets": [2]
                }
            ],
			
            //"dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable

            // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
            // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js). 
            // So when dropdowns used the scrollable div should be removed. 
            //"dom": "<'row' <'col-md-12'T>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",
        });
		
		// handle datatable custom tools
        $('#sample_1_tools > li > a.tool-action').on('click', function() {
            var action = $(this).attr('data-action');
            oTable.DataTable().button(action).trigger();
        });
		
		
    }
	
	var initTable1 = function () {
        var table = $('#tipoincidentestable');

        var oTable = table.dataTable({

            // Internationalisation. For more info refer to http://datatables.net/manual/i18n
            "language": {
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "Nenhum dado a ser exibido.",
                "info": "Mostrando _START_ até _END_ de _TOTAL_ registros",
                "infoEmpty": "Nenhum dado foi encontrado",
                "infoFiltered": "(filtered1 de _MAX_ resultados totais)",
                "lengthMenu": "_MENU_ Registros",
                "search": "Pesquisar:",
                "zeroRecords": "A busca não encontrou nada."
            },

            // Or you can use remote translation file
            //"language": {
            //   url: '//cdn.datatables.net/plug-ins/3cfcc339e89/i18n/Portuguese.json'
            //},


            buttons: [
                { extend: 'print', text: '<i class="icon-printer"></i> Imprimir', className: 'btn dark', 
				exportOptions: {
                    columns: [ 0, 1 ]
                },
				customize: function ( win ) {
                    $(win.document.body)
                        .css( 'font-size', '10pt' );
 
                    $(win.document.body).find( 'table' )
                        .addClass( 'compact' )
                        .css( 'font-size', 'inherit' );
                }
				},
                { extend: 'pdf', text:'<i class="icon-doc"></i> PDF', className: 'btn green', title: 'empresas', 
				exportOptions: {
                    columns: [ 0, 1 ]
                }},
                { extend: 'excel', text:'<i class="icon-paper-clip"></i> EXCEL', className: 'btn yellow', title: 'empresas',
				exportOptions: {
                    columns: [ 0, 1 ]
                }}
				/*,
                { extend: 'csv', className: 'btn purple', title: 'lista-admin-web',
				exportOptions: {
                    columns: [ 1, 2, 3, 4, 5 ]
                }}*/
            ],
			
			
			
            // setup responsive extension: http://datatables.net/extensions/responsive/
            responsive: true,

            "ordering": false,
            "paging": false,
			"bInfo" : false,
            "order": [
                [0, 'asc']
            ],
            
            "lengthMenu": [
                [1, 10, 15, 20, -1],
                [5, 10, 15, 20, "All"] // change per page values here
            ],
			lengthChange: false,
            // set the initial value
            "pageLength": 20,
			"columnDefs": [
                {  // set default column settings
                    'orderable': false,
                    'targets': [0]
                }, 
                {
                    "searchable": false,
                    "targets": [0]
                },
                {
                    "className": "dt-right", 
                    //"targets": [2]
                }
            ],
			
            //"dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable

            // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
            // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js). 
            // So when dropdowns used the scrollable div should be removed. 
            //"dom": "<'row' <'col-md-12'T>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",
        });
		
		// handle datatable custom tools
        $('#sample_1_tools > li > a.tool-action').on('click', function() {
            var action = $(this).attr('data-action');
            oTable.DataTable().button(action).trigger();
        });
		
		
    }
	
	
    var initTable2 = function () {
        var table = $('#modalidades');

        var oTable = table.dataTable({

            // Internationalisation. For more info refer to http://datatables.net/manual/i18n
            "language": {
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "Nenhum dado a ser exibido.",
                "info": "Mostrando _START_ até _END_ de _TOTAL_ registros",
                "infoEmpty": "Nenhum dado foi encontrado",
                "infoFiltered": "(filtered1 de _MAX_ resultados totais)",
                "lengthMenu": "_MENU_ Registros",
                "search": "Pesquisar:",
                "zeroRecords": "A busca não encontrou nada."
            },

            // Or you can use remote translation file
            //"language": {
            //   url: '//cdn.datatables.net/plug-ins/3cfcc339e89/i18n/Portuguese.json'
            //},


            buttons: [
                { extend: 'print', text: '<i class="icon-printer"></i> Imprimir', className: 'btn dark', 
				exportOptions: {
                    columns: [ 0, 1 ]
                },
				customize: function ( win ) {
                    $(win.document.body)
                        .css( 'font-size', '10pt' );
 
                    $(win.document.body).find( 'table' )
                        .addClass( 'compact' )
                        .css( 'font-size', 'inherit' );
                }
				},
                { extend: 'pdf', text:'<i class="icon-doc"></i> PDF', className: 'btn green', title: 'lista-admin-web', 
				exportOptions: {
                    columns: [ 0, 1 ]
                }},
                { extend: 'excel', text:'<i class="icon-paper-clip"></i> EXCEL', className: 'btn yellow', title: 'lista-admin-web',
				exportOptions: {
                    columns: [ 0, 1 ]
                }}
				/*,
                { extend: 'csv', className: 'btn purple', title: 'lista-admin-web',
				exportOptions: {
                    columns: [ 1, 2, 3, 4, 5 ]
                }}*/
            ],
			
			
			
            // setup responsive extension: http://datatables.net/extensions/responsive/
            responsive: true,

            "ordering": false,
            "paging": false,
			"bInfo" : false,
            "order": [
                [0, 'asc']
            ],
            
            "lengthMenu": [
                [1, 10, 15, 20, -1],
                [5, 10, 15, 20, "All"] // change per page values here
            ],
			lengthChange: false,
            // set the initial value
            "pageLength": 20,
			"columnDefs": [
                {  // set default column settings
                    'orderable': false,
                    'targets': [0]
                }, 
                {
                    "searchable": false,
                    "targets": [0]
                },
                {
                    "className": "dt-right", 
                    //"targets": [2]
                }
            ],
			
            //"dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable

            // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
            // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js). 
            // So when dropdowns used the scrollable div should be removed. 
            //"dom": "<'row' <'col-md-12'T>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",
        });
		
		// handle datatable custom tools
        $('#sample_1_tools > li > a.tool-action').on('click', function() {
            var action = $(this).attr('data-action');
            oTable.DataTable().button(action).trigger();
        });
		
    }
	
	var initTable10 = function () {
        var table = $('#licitantes');

        var oTable = table.dataTable({

            // Internationalisation. For more info refer to http://datatables.net/manual/i18n
            "language": {
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "Nenhum dado a ser exibido.",
                "info": "Mostrando _START_ até _END_ de _TOTAL_ registros",
                "infoEmpty": "Nenhum dado foi encontrado",
                "infoFiltered": "(filtered1 de _MAX_ resultados totais)",
                "lengthMenu": "_MENU_ Registros",
                "search": "Pesquisar:",
                "zeroRecords": "A busca não encontrou nada."
            },

            // Or you can use remote translation file
            //"language": {
            //   url: '//cdn.datatables.net/plug-ins/3cfcc339e89/i18n/Portuguese.json'
            //},


            buttons: [
                { extend: 'print', text: '<i class="icon-printer"></i> Imprimir', className: 'btn dark', 
				exportOptions: {
                    columns: [ 0, 2, 3,4,5,6,7 ]
                },
				customize: function ( win ) {
                    $(win.document.body)
                        .css( 'font-size', '10pt' );
 
                    $(win.document.body).find( 'table' )
                        .addClass( 'compact' )
                        .css( 'font-size', 'inherit' );
                }
				},
                { extend: 'pdf', text:'<i class="icon-doc"></i> PDF', className: 'btn green', title: 'lista-admin-web', 
				exportOptions: {
                    columns: [ 0, 2, 3,4,5,6,7 ]
                }},
                { extend: 'excel', text:'<i class="icon-paper-clip"></i> EXCEL', className: 'btn yellow', title: 'lista-admin-web',
				exportOptions: {
                    columns: [ 0, 2, 3,4,5,6,7 ]
                }}
				/*,
                { extend: 'csv', className: 'btn purple', title: 'lista-admin-web',
				exportOptions: {
                    columns: [ 1, 2, 3, 4, 5 ]
                }}*/
            ],
			
			
			
            // setup responsive extension: http://datatables.net/extensions/responsive/
            responsive: true,

            "ordering": true,
            "paging": true,
			"bInfo" : false,
			/*
            "order": [
                [0, 'asc']
            ],
            */
            "lengthMenu": [
                [1, 10, 15, 20, -1],
                [5, 10, 15, 20, "All"] // change per page values here
            ],
			lengthChange: false,
            // set the initial value
            "pageLength": 20,
			"columnDefs": [
                {  // set default column settings
                    'orderable': false,
                    'targets': [0]
                }, 
                {
                    "searchable": false,
                    "targets": [0]
                },
                {
                    "className": "dt-right", 
                    //"targets": [2]
                }
            ],
			
            //"dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable

            // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
            // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js). 
            // So when dropdowns used the scrollable div should be removed. 
            //"dom": "<'row' <'col-md-12'T>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",
        });
		
		// handle datatable custom tools
        $('#sample_1_tools > li > a.tool-action').on('click', function() {
            var action = $(this).attr('data-action');
            oTable.DataTable().button(action).trigger();
        });
		
    }
	
	var initTable11 = function () {
		var today = new Date();
		var dd = today.getDate();
		var mm = today.getMonth()+1; //January is 0!
		var yyyy = today.getFullYear();

		if(dd<10) {
			dd='0'+dd
		} 

		if(mm<10) {
			mm='0'+mm
		} 

		today = dd+'-'+mm+'-'+yyyy;
        var table = $('#relatorios');

        var oTable = table.dataTable({

            // Internationalisation. For more info refer to http://datatables.net/manual/i18n
            "language": {
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "Nenhum dado a ser exibido.",
                "info": "Mostrando _START_ até _END_ de _TOTAL_ registros",
                "infoEmpty": "Nenhum dado foi encontrado",
                "infoFiltered": "(filtered1 de _MAX_ resultados totais)",
                "lengthMenu": "_MENU_ Registros",
                "search": "Pesquisar:",
                "zeroRecords": "A busca não encontrou nada."
            },

            // Or you can use remote translation file
            //"language": {
            //   url: '//cdn.datatables.net/plug-ins/3cfcc339e89/i18n/Portuguese.json'
            //},


            buttons: [
                { extend: 'print', text: '<i class="icon-printer"></i> Imprimir', className: 'btn dark', 
				exportOptions: {
                    columns: [ 0, 1, 2,3,4 ]
                },
				customize: function ( win ) {
                    $(win.document.body)
                        .css( 'font-size', '10pt' );
 
                    $(win.document.body).find( 'table' )
                        .addClass( 'compact' )
                        .css( 'font-size', 'inherit' );
                }
				},
                { extend: 'pdf', text:'<i class="icon-doc"></i> PDF', className: 'btn green', title: 'relatorios-admin-web-'+today, 
				exportOptions: {
                    columns: [ 0, 1, 2,3,4 ]
                }},
                { extend: 'excel', text:'<i class="icon-paper-clip"></i> EXCEL', className: 'btn yellow', title: 'relatorios-admin-web-'+today,
				exportOptions: {
                    columns: [ 0, 1, 2,3,4 ]
                }}
				/*,
                { extend: 'csv', className: 'btn purple', title: 'lista-admin-web',
				exportOptions: {
                    columns: [ 1, 2, 3, 4, 5 ]
                }}*/
            ],
			
			
			
            // setup responsive extension: http://datatables.net/extensions/responsive/
            responsive: true,

            "ordering": true,
            "paging": true,
			"bInfo" : false,
			/*
            "order": [
                [0, 'asc']
            ],
            */
            "lengthMenu": [
                [1, 10, 15, 20, -1],
                [5, 10, 15, 20, "All"] // change per page values here
            ],
			lengthChange: false,
            // set the initial value
            "pageLength": 20,
			"columnDefs": [
                {  // set default column settings
                    'orderable': false,
                    'targets': [0]
                }, 
                {
                    "searchable": false,
                    "targets": [0]
                },
                {
                    "className": "dt-right", 
                    //"targets": [2]
                }
            ],
			
            //"dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable

            // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
            // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js). 
            // So when dropdowns used the scrollable div should be removed. 
            //"dom": "<'row' <'col-md-12'T>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",
        });
		
		// handle datatable custom tools
        $('#sample_1_tools > li > a.tool-action').on('click', function() {
            var action = $(this).attr('data-action');
            oTable.DataTable().button(action).trigger();
        });
		
    }

    var initTable3 = function () {
        var table = $('#licitacao');

        var oTable = table.dataTable({

            // Internationalisation. For more info refer to http://datatables.net/manual/i18n
            "language": {
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "Nenhum dado a ser exibido.",
                "info": "Mostrando _START_ até _END_ de _TOTAL_ registros",
                "infoEmpty": "Nenhum dado foi encontrado",
                "infoFiltered": "(filtered1 de _MAX_ resultados totais)",
                "lengthMenu": "_MENU_ Registros",
                "search": "Pesquisar:",
                "zeroRecords": "A busca não encontrou nada."
            },

            // Or you can use remote translation file
            //"language": {
            //   url: '//cdn.datatables.net/plug-ins/3cfcc339e89/i18n/Portuguese.json'
            //},


            buttons: [
                { extend: 'print', text: '<i class="icon-printer"></i> Imprimir', className: 'btn dark', 
				exportOptions: {
                    columns: [ 0, 1, 2, 3, 4, 5]
                },
				customize: function ( win ) {
                    $(win.document.body)
                        .css( 'font-size', '10pt' );
 
                    $(win.document.body).find( 'table' )
                        .addClass( 'compact' )
                        .css( 'font-size', 'inherit' );
                }
				},
                { extend: 'pdf', text:'<i class="icon-doc"></i> PDF', className: 'btn green', title: 'lista-admin-web', 
				exportOptions: {
                    columns: [ 0, 1, 2, 3, 4, 5]
                }},
                { extend: 'excel', text:'<i class="icon-paper-clip"></i> EXCEL', className: 'btn yellow', title: 'lista-admin-web',
				exportOptions: {
                    columns: [ 0, 1, 2, 3, 4, 5]
                }}
				/*,
                { extend: 'csv', className: 'btn purple', title: 'lista-admin-web',
				exportOptions: {
                    columns: [ 1, 2, 3, 4, 5 ]
                }}*/
            ],
			
			
			
            // setup responsive extension: http://datatables.net/extensions/responsive/
            responsive: true,

            //"ordering": false,
            //"paging": false,
			//"bInfo" : false,
            "order": [
                [0, 'asc']
            ],
           "columns": [
				{ "width": "10%" },
				{ "width": "10%" },
				{ "width": "20%" },
				{ "width": "30%" },
				{ "width": "30%" },
				{ "width": "5%" },
				{ "width": "5%" }
			],
            "lengthMenu": [
                [1, 10, 15, 20, -1],
                [5, 10, 15, 20, "All"] // change per page values here
            ],
			//lengthChange: false,
            // set the initial value
            "pageLength": 20,
			
            //"dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable

            // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
            // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js). 
            // So when dropdowns used the scrollable div should be removed. 
            //"dom": "<'row' <'col-md-12'T>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",
        });
		
		// handle datatable custom tools
        $('#sample_1_tools > li > a.tool-action').on('click', function() {
            var action = $(this).attr('data-action');
            oTable.DataTable().button(action).trigger();
        });
    }

    var initAjaxDatatables = function () {

        //init date pickers
        $('.date-picker').datepicker({
            rtl: App.isRTL(),
            autoclose: true
        });

        var grid = new Datatable();

        grid.init({
            src: $("#datatable_ajax"),
            onSuccess: function (grid, response) {
                // grid:        grid object
                // response:    json object of server side ajax response
                // execute some code after table records loaded
            },
            onError: function (grid) {
                // execute some code on network or other general error  
            },
            onDataLoad: function(grid) {
                // execute some code on ajax data load
            },
            loadingMessage: 'Loading...',
            dataTable: { // here you can define a typical datatable settings from http://datatables.net/usage/options 

                // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
                // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/scripts/datatable.js). 
                // So when dropdowns used the scrollable div should be removed. 
                
                //"dom": "<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'<'table-group-actions pull-right'>>r>t<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'>>",
                
                "bStateSave": true, // save datatable state(pagination, sort, etc) in cookie.

                "lengthMenu": [
                    [10, 20, 50, 100, 150, -1],
                    [10, 20, 50, 100, 150, "All"] // change per page values here
                ],
                "pageLength": 10, // default record count per page
                "ajax": {
                    "url": "../demo/table_ajax.php", // ajax source
                },
                "order": [
                    [1, "asc"]
                ],// set first column as a default sort by asc
            
                // Or you can use remote translation file
                //"language": {
                //   url: '//cdn.datatables.net/plug-ins/3cfcc339e89/i18n/Portuguese.json'
                //},

                buttons: [
                    { extend: 'print', className: 'btn default' },
                    { extend: 'copy', className: 'btn default' },
                    { extend: 'pdf', className: 'btn default' },
                    { extend: 'excel', className: 'btn default' },
                    { extend: 'csv', className: 'btn default' },
                    {
                        text: 'Reload',
                        className: 'btn default',
                        action: function ( e, dt, node, config ) {
                            dt.ajax.reload();
                            alert('Datatable reloaded!');
                        }
                    }
                ],

            }
        });

        // handle group actionsubmit button click
        grid.getTableWrapper().on('click', '.table-group-action-submit', function (e) {
            e.preventDefault();
            var action = $(".table-group-action-input", grid.getTableWrapper());
            if (action.val() != "" && grid.getSelectedRowsCount() > 0) {
                grid.setAjaxParam("customActionType", "group_action");
                grid.setAjaxParam("customActionName", action.val());
                grid.setAjaxParam("id", grid.getSelectedRows());
                grid.getDataTable().ajax.reload();
                grid.clearAjaxParams();
            } else if (action.val() == "") {
                App.alert({
                    type: 'danger',
                    icon: 'warning',
                    message: 'Please select an action',
                    container: grid.getTableWrapper(),
                    place: 'prepend'
                });
            } else if (grid.getSelectedRowsCount() === 0) {
                App.alert({
                    type: 'danger',
                    icon: 'warning',
                    message: 'No record selected',
                    container: grid.getTableWrapper(),
                    place: 'prepend'
                });
            }
        });

        //grid.setAjaxParam("customActionType", "group_action");
        //grid.getDataTable().ajax.reload();
        //grid.clearAjaxParams();

        // handle datatable custom tools
        $('#datatable_ajax_tools > li > a.tool-action').on('click', function() {
            var action = $(this).attr('data-action');
            grid.getDataTable().button(action).trigger();
        });
    }

    return {

        //main function to initiate the module
        init: function () {

            if (!jQuery().dataTable) {
                return;
            }

            initTable1();
            initTable2();
            initTable3();
            initTable4();
            initTable10();
			initTable11();

            initAjaxDatatables();
        }

    };

}();

jQuery(document).ready(function() {
    TableDatatablesButtons.init();
});