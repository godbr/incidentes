﻿########################################################################################

CRUD CRIADO UTILIZANDO :
- CODE IGNITER 3
- BANCO DE DADOS MYSQL
- BOOTSTRAP

Arquivo de Banco de dados encontra-se na raiz do projeto com o nome bancodedados.sql 

Para Configuração e Instalação:
	/application/config/config.php
	Linha 26: Alterar o PATH para a pasta que estiver utilizando
	
	$config['base_url'] = 'http://seu.local/pasta/';
	
	/application/config/database.php
	Linha 76: Alterar as configurações para o Banco de Dados
	
	$db['default'] = array(
		'dsn'	=> '',
		'hostname' => 'localhost',
		'username' => 'root',
		'password' => '',
		'database' => 'banco_teste',
		'dbdriver' => 'mysqli',
		'dbprefix' => '',
		'pconnect' => FALSE,
		'db_debug' => (ENVIRONMENT !== 'production'),
		'cache_on' => FALSE,
		'cachedir' => '',
		'char_set' => 'utf8',
		'dbcollat' => 'utf8_general_ci',
		'swap_pre' => '',
		'encrypt' => FALSE,
		'compress' => FALSE,
		'stricton' => FALSE,
		'failover' => array(),
		'save_queries' => TRUE
	);
	
########################################################################################